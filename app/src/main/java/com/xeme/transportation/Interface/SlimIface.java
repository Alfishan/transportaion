package com.xeme.transportation.Interface;

import com.xeme.transportation.POJO.Expensedataresponse;
import com.xeme.transportation.POJO.GetdataBeforeStopTrip;
import com.xeme.transportation.POJO.LogLatLongresponse;
import com.xeme.transportation.POJO.LoginModel;
import com.xeme.transportation.POJO.LogoutModel;
import com.xeme.transportation.POJO.Refuelingdataresponse;
import com.xeme.transportation.POJO.ReqIMEI;
import com.xeme.transportation.POJO.Servicedataresponse;
import com.xeme.transportation.POJO.StartDriverTripModel;
import com.xeme.transportation.POJO.Starttripdataresponse;
import com.xeme.transportation.POJO.TripLogsresponse;
import com.xeme.transportation.POJO.smsAssignTripresponse;
import com.xeme.transportation.POJO.smsdataresponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by root on 5/7/16.
 */
public interface SlimIface {


    @FormUrlEncoded
    @POST("/tm/v1/logindd")
    Call<LoginModel> StartStopTrip(@Field("username") String username,
                                   @Field("password") String password,
                                   @Field("userid") int userid);

    @FormUrlEncoded
    @POST("/tm/v1/Logoutdd")
    Call<LogoutModel>Logoutapp(@Field("cid") int cid,
                               @Field("did") int did);

    //for start trip.
    @FormUrlEncoded
    @POST("/tm/v1/startDriverTrip")
    Call<StartDriverTripModel>StartDriverTrip(@Field("did") int did,
                                              @Field("cid") int cid,
                                              @Field("start_location") String startlocation,
                                              @Field("initial_odometer") String initialodometer,
                                              @Field("date_time_onstart") String datetimeonstart,
                                              @Field("destination_location") String destinationlocation,
                                              @Field("reason") String reason,
                                              @Field("extra_notes_onstart") String extranotesonstart,
                                              @Field("packageimg") String packageimg,
                                              @Field("is_start") int istart,
                                              @Field("vid") int vid);


    //for insert service..
    @FormUrlEncoded
    @POST("/tm/v1/service")
    Call<StartDriverTripModel>Serviceinsert(@Field("cid") int cid,
                                            @Field("did") int did,
                                            @Field("tripid") int tripid,
                                            @Field("datetime") String datetime,
                                            @Field("odometer") String odometer,
                                            @Field("value") String value,
                                            @Field("servicetype") String servicetype,
                                            @Field("extranotes") String extranotes,
                                            @Field("latitude") double lat,
                                            @Field("langitude") double lang,
                                            @Field("vid") int vid);



    //for insert expense\
    @FormUrlEncoded
    @POST("/tm/v1/expenses")
    Call<StartDriverTripModel>Expenseinsert(@Field("cid") int cid,
                                            @Field("did") int did,
                                            @Field("tripid") int tripid,
                                            @Field("datetime") String datetime,
                                            @Field("odometer") String odometer,
                                            @Field("name_of_expenses") String nameofexpense,
                                            @Field("value") String value,
                                            @Field("expenses_type") String expensetype,
                                            @Field("reason") String reason,
                                            @Field("extranotes") String extranotes,
                                            @Field("latitude") double lat,
                                            @Field("langitude") double lang,
                                            @Field("vid") int vid);

    //for insert refueling
    @FormUrlEncoded
    @POST("/tm/v1/refueling")
    Call<StartDriverTripModel>Refuellinginsert(@Field("cid") int cid,
                                               @Field("did") int did,
                                               @Field("tripid") int tripid,
                                               @Field("datetime") String datetime,
                                               @Field("odometer") String odometer,
                                               @Field("price_l") String price_l,
                                               @Field("totalprice") String totalprice,
                                               @Field("liters") String liters,
                                               @Field("is_full") int is_full,
                                               @Field("fuel_type") String fuel_type,
                                               @Field("station_type") String stationtype,
                                               @Field("reason") String reason,
                                               @Field("extra_notes") String extranotes,
                                               @Field("latitude") double lat,
                                               @Field("langitude") double lang,
                                               @Field("vid") int vid);

    //For Getting Data for stop strip
    @FormUrlEncoded
    @POST("/tm/v1/getdataforstoptrip")
    Call<GetdataBeforeStopTrip> GetforStop(@Field("did") int did,
                                           @Field("cid") int cid,
                                           @Field("tripid") int tripid);

    //for Stop trip
    @FormUrlEncoded
    @POST("/tm/v1/" +
            "")
    Call<StartDriverTripModel>StopDriverTrip(@Field("tripid") int tripid,
                                             @Field("did") int did,
                                             @Field("cid") int cid,
                                             @Field("finalodometer") String finalodometer,
                                             @Field("enddate") String enddate,
                                             @Field("packageimg") String packageimg,
                                             @Field("extranotesonstop") String extranotes,
                                             @Field("is_start") int istart);

    @FormUrlEncoded
    @POST("/tm/v1/GetStarttripData")
    Call<Starttripdataresponse>getStartdata(@Field("driverid") int did);

//    @GET("/tm/v1/GetStarttripData?driverid=34")
//    Call<Starttripdataresponse>getStartdata();//Callback<List<Refuelingdata>> response

    @FormUrlEncoded
    @POST("/tm/v1/GetTtripLogsData")
    Call<TripLogsresponse>getTripLogsdata(@Field("did") int did);

    @FormUrlEncoded
    @POST("/tm/v1/GetServiceData")
    Call<Servicedataresponse>getServicedata(@Field("did") int did);

    @FormUrlEncoded
    @POST("/tm/v1/GetExpenseData")
    Call<Expensedataresponse>getExpensedata(@Field("did") int did);

    @FormUrlEncoded
    @POST("/tm/v1/GetRefuelingData")
    Call<Refuelingdataresponse>getRefuelingdata(@Field("did") int did);

    @FormUrlEncoded
    @POST("/tm/v1/GetLatLongLogs")
    Call<LogLatLongresponse>GetLogsLatLong(@Field("did") int did);

    @FormUrlEncoded
    @POST("/tm/v1/getsms")
    Call<smsdataresponse>getsmsdata(@Field("did") int did);

    @FormUrlEncoded
    @POST("/tm/v1/getassignedtrip")
    Call<smsAssignTripresponse>getassigntripdata(@Field("did") int did);

    //For Req for New Device..
    @FormUrlEncoded
    @POST("/tm/v1/IMEIReq")
    Call<ReqIMEI> ReqIMEI(@Field("userid") int userid,
                          @Field("imei") String imei,
                          @Field("operator") String operator,
                          @Field("firebaseid") String firebaseid);
    @FormUrlEncoded
    @POST("/tm/v1/addpanic")
    Call<LogoutModel>AddPanic(@Field("did") int did,
                               @Field("vid") int vid,
                              @Field("panictype") String panictype,
                              @Field("panicdetail") String panicdetail,
                              @Field("panicimg") String panicimg);

}
