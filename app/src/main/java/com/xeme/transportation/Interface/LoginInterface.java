package com.xeme.transportation.Interface;

import com.xeme.transportation.POJO.LoginModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by root on 2/7/16.
 */
public interface LoginInterface {

    @FormUrlEncoded
    @POST("/tm/v1/logindd")
    Call<LoginModel> GetLoginDetails(@Field("username") String username,
                                     @Field("password") String password,
                                     @Field("userid") int userid,
                                     @Field("imei") String imei,
                                     @Field("operator") String operator,
                                     @Field("firebaseid") String firebaseid);

    @FormUrlEncoded
    @POST("/tm/v1/logoutdd")
    Call<LoginModel> Logout(@Field("userid") int userid);

    @FormUrlEncoded
    @POST("/logindd")
    Call<LoginModel> Login(@Field("username") String username,
                           @Field("password") String pass,
                           @Field("user_id") int user_id);

    @FormUrlEncoded
    @POST("/signup")
    void SignUp(@Field("name") String name, @Field("email") String email,
                @Field("pass") String pass, Callback<LoginModel> pm);

//    @GET("api/{username}/{password}/{driverid}")
//    Call<LoginInterface> authenticate(@Path("username") String email, @Path("password") String password, @Path("password") int driverid);
//    @POST("api/{email}/{password}")
//    Call<LoginInterface> registration(@Path("email") String email, @Path("password") String password);

//    @FormUrlEncoded
//    @POST("/BusinessVocabulary/register.php")
//    public void insertUser(
//            @Field("username") String username,
//            @Field("email") String email,
//            @Field("password") String password,
//            @Field("country") String country,
//            @Field("country") String gender,
//            @Field("country") String age,
//            @Field("country") String level,
//            @Field("country") String sector,
//            Callback<Response> callback);
}
