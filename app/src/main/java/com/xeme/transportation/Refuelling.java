package com.xeme.transportation;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.xeme.transportation.DateTimePicker.SlideDateTimeListener;
import com.xeme.transportation.DateTimePicker.SlideDateTimePicker;
import com.xeme.transportation.Interface.SlimIface;
import com.xeme.transportation.POJO.StartDriverTripModel;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.Gpslocation;
import com.xeme.transportation.utils.MySharedPreferences;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Refuelling extends AppCompatActivity {

    String[] reason = {"Reason","Trip","Work"};
    String[] fuel = {"Diesel","Ethanol","Gas Premium","Gasolin"};
    String[] fuellocation = {"Gas Station"};
    int isfull = 0;

    EditText edtrefuelingdate,edtrefueilngodometer,edtrefuelingprice_l,edtrefuelingtotalprice,edtrefuelingliters,edtrefuelingnotes;
    Button btninsertrefueling;
    Spinner spinnerrefuellingreson,spinnerrefuellingfuel,spinnerrefuellingfuellocation;
    CheckBox cbisfull;
    private SimpleDateFormat mFormatter = new SimpleDateFormat("dd MM yyyy hh:mm aa");
    MySharedPreferences mypref;
    int cID,dID,tripID,vid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refuelling);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);

        mypref = MySharedPreferences.getInstance(this, "Transportation");
        cID = mypref.getInt(Constants.KEY_CUSTOMER_ID,0);
        dID = mypref.getInt(Constants.KEY_DRIVER_ID,0);
        tripID = mypref.getInt(Constants.KEY_TRIP_ID,0);
        vid = mypref.getInt(Constants.KEY_VEHICLE_ID,0);

        edtrefuelingdate = (EditText) findViewById(R.id.RefuellingDate);
        edtrefueilngodometer = (EditText)findViewById(R.id.RefuellingOdometer);
        edtrefuelingprice_l = (EditText)findViewById(R.id.Refuellingpriceper);
        edtrefuelingtotalprice = (EditText)findViewById(R.id.Refuellingpricetotal);
        edtrefuelingliters = (EditText)findViewById(R.id.Refuellingfuelliters);
        edtrefuelingnotes = (EditText)findViewById(R.id.RefuellingExtranote);

        btninsertrefueling = (Button)findViewById(R.id.btn_Refuellingsubmit);

        spinnerrefuellingfuel = (Spinner) findViewById(R.id.SpinnerRefuellingfuel);
        spinnerrefuellingfuellocation = (Spinner) findViewById(R.id.SpinnerRefuellingfuelstation);
        spinnerrefuellingreson = (Spinner) findViewById(R.id.SpinnerRefuellingjourney);
        cbisfull = (CheckBox)findViewById(R.id.refuelingcheckbox);

        ArrayAdapter<String> adapterfuel = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, fuel);
        spinnerrefuellingfuel.setAdapter(adapterfuel);
        ArrayAdapter<String> adapterfuellocation = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, fuellocation);
        spinnerrefuellingfuellocation.setAdapter(adapterfuellocation);
        ArrayAdapter<String> adapterreson = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, reason);
        spinnerrefuellingreson.setAdapter(adapterreson);

        edtrefuelingdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        //.setMinDate(minDate)
                        //.setMaxDate(maxDate)
                        //.setIs24HourTime(true)
                        //.setTheme(SlideDateTimePicker.HOLO_DARK)
                        //.setIndicatorColor(Color.parseColor("#990000"))
                        .build()
                        .show();
            }
        });

        btninsertrefueling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gpslocation myloc = new Gpslocation(Refuelling.this);
                LatLng l = myloc.getlatlong();
                double lat = l.latitude;
                double lng = l.longitude;

                //Toast.makeText(Refuelling.this, "lat : " + lat + " Lng : " + lng, Toast.LENGTH_SHORT).show();
               // System.out.println("lat : " + lat + " Lng : " + lng);

                if (CheckFieldValidation()){

                    String refuelingdate = edtrefuelingdate.getText().toString();
                    String refueilngodometer = edtrefueilngodometer.getText().toString();
                    String refuelingprice_l = edtrefuelingprice_l.getText().toString();
                    String refuelingtotalprice = edtrefuelingtotalprice.getText().toString();
                    String refuelingliters = edtrefuelingliters.getText().toString();
                    String refuelingnotes = edtrefuelingnotes.getText().toString();
                    if (cbisfull.isChecked()){
                        isfull = 1;
                    }
                    else{
                        isfull = 0;
                    }
                    String refuelingfueltype = spinnerrefuellingfuel.getSelectedItem().toString();
                    String refuelingstation = spinnerrefuellingfuellocation.getSelectedItem().toString();
                    String refuelingreason = spinnerrefuellingreson.getSelectedItem().toString();


                    InsertRefueling(cID,dID,tripID,refuelingdate,refueilngodometer,refuelingprice_l,refuelingtotalprice,refuelingliters,isfull,refuelingfueltype,refuelingstation,refuelingreason,refuelingnotes,lat,lng,vid);
                }
            }
        });

    }
    private boolean CheckFieldValidation(){
        boolean valid=true;
        if(edtrefuelingdate.getText().toString().equals("")){
            edtrefuelingdate.setError("no puede estar vacía");
            valid=false;
        }else if(edtrefueilngodometer.getText().toString().equals("")){
            edtrefueilngodometer.setError("no puede estar vacía");
            valid=false;
        }else if (edtrefuelingprice_l.getText().toString().equals("")){
            edtrefuelingprice_l.setError("no puede estar vacía");
            valid=false;
        }else if (edtrefuelingtotalprice.getText().toString().equals("")){
            edtrefuelingtotalprice.setError("no puede estar vacía");
            valid=false;
        }else if (edtrefuelingliters.getText().toString().equals("")){
            edtrefuelingliters.setError("no puede estar vacía");
            valid=false;
        }else if (edtrefuelingnotes.getText().toString().equals("")){
            edtrefuelingnotes.setError("no puede estar vacía");
            valid=false;
        }
        return valid;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }
    private SlideDateTimeListener listener = new SlideDateTimeListener() {

        @Override
        public void onDateTimeSet(Date date)
        {
            edtrefuelingdate.setText(mFormatter.format(date));
            //Toast.makeText(Refuelling.this,mFormatter.format(date), Toast.LENGTH_SHORT).show();
        }

        // Optional cancel listener
        @Override
        public void onDateTimeCancel()
        {
            Toast.makeText(Refuelling.this,
                    "Cancelado", Toast.LENGTH_SHORT).show();
        }
    };
    public void InsertRefueling(int cid,int did,int tripid,String srefuelingdate,String sinitialodometer,String srefuelingprice_l,String srefuelingtotalprice,String srefuelingliters,int iisfull,String srefuelingfueltype,String srefuelingstation,String srefuelingreason,String srefuelingnotes,double lat,double lang,int vid){

        final ProgressDialog loading = ProgressDialog.show(this,"Cargando","por favor espera...",false,false);
//        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
//        final SharedPreferences.Editor editor = pref.edit();

        // OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create())
                // .client(httpClient.build())
                .build();

        SlimIface service = retrofit.create(SlimIface.class);
        Call<StartDriverTripModel> call = service.Refuellinginsert(cid,did,tripid,srefuelingdate,sinitialodometer,srefuelingprice_l,srefuelingtotalprice,srefuelingliters,iisfull,srefuelingfueltype,srefuelingstation,srefuelingreason,srefuelingnotes,lat,lang,vid);
        call.enqueue(new Callback<StartDriverTripModel>() {
            @Override
            public void onResponse(Call<StartDriverTripModel> call, Response<StartDriverTripModel> response) {

                if (response != null) {
                    Log.wtf("Transport insert Refueling", "Response: "+response.message());
                    if (response.body().error.equals(false)) {  //insert Success
                        loading.dismiss();
                        Toast.makeText(Refuelling.this, "Reabastecimiento de combustible insertado con éxito", Toast.LENGTH_SHORT).show();
//                        mypref.putInt(Constants.KEY_TRIP_ID, response.body().tripid);
//                        mypref.commit();
                        onBackPressed();

                    } else // insert failure
                    {
                        loading.dismiss();
                        Toast.makeText(Refuelling.this, "Por favor, rellene los datos correctamente", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    loading.dismiss();
                    Toast.makeText(Refuelling.this,response.body().toString() , Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<StartDriverTripModel> call, Throwable t) {
                loading.dismiss();
                String merror = t.getMessage();
                Toast.makeText(Refuelling.this, merror, Toast.LENGTH_LONG).show();
                Log.wtf("Transport", "onFailure: "+t.getCause());
            }
        });
    }

}
