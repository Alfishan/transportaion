package com.xeme.transportation;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.xeme.transportation.Interface.LoginInterface;
import com.xeme.transportation.Interface.SlimIface;
import com.xeme.transportation.POJO.LoginModel;
import com.xeme.transportation.POJO.ReqIMEI;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.MyLocationService;
import com.xeme.transportation.utils.MySharedPreferences;

import java.util.HashMap;
import java.util.Map;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Login extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 0;
    private TextView loading_tv2;
    String IMEINumber,NetworkProviderName,phonenumber;
    EditText edtname,edtpass,edtuid;
    SharedPreferences Pref_DId;
    MySharedPreferences mypref;
    Button btnlogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            new AlertDialog.Builder(this)
                    .setTitle("Solicitud de permiso")
                    .setMessage("Por favor, activa el GPS")
                    .setPositiveButton("De acuerdo", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    })
                    .setNegativeButton("Cancelado", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

        btnlogin = (Button)findViewById(R.id.btnlogin);
        edtname = (EditText)findViewById(R.id.edtusername);
        edtpass = (EditText)findViewById(R.id.edtpassword);
        edtuid = (EditText)findViewById(R.id.edtuniqueid);

//        Pref_DId = getApplicationContext().getSharedPreferences("MyDid", MODE_PRIVATE);
//        final SharedPreferences.Editor editor = Pref_DId.edit();
        mypref = MySharedPreferences.getInstance(this,"Transportation");

//        final List<TableModel> users = new Select().from(TableModel.class).queryList();
//
//        StringBuilder builder = new StringBuilder();
//        for (TableModel person : users) {
//            builder.append("Name: ")
//                    .append(person.name)
//                    .append(" UID: ")
//                    .append(person.id)
//                    .append("imei :")
//                    .append(person.imei)
//                    .append("coreer Pro :" + person.careerprovider)
//                    .append("\n");
//        }
     //   Toast.makeText(this, builder.toString(), Toast.LENGTH_LONG).show();



//        .select()
//                .from(Automobile.class)
//                .where(Automobile_Table.year.is(2001))
//                .and(Automobile_Table.model.is("Camry"))
//                .async()
//                .queryResultCallback(new QueryTransaction.QueryResultCallback<TestModel1>() {
//                    @Override
//                    public void onQueryResult(QueryTransaction transaction, @NonNull CursorResult<TestModel1> tResult) {
//                        // called when query returns on UI thread
//                        List<Automobile> autos = tResult.toListClose();
//                        // do something with results
//                    }
//                }, new Transaction.Error() {
//                    @Override
//                    public void onError(Transaction transaction, Throwable error) {
//                        // handle any errors
//                    }
//                }).execute();


//        String deviceid;
//        deviceid = DeviceID.getID();
//        Toast.makeText(getApplicationContext(),"DeviceID : " + deviceid,Toast.LENGTH_LONG).show();

        loadIMEI();
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             /*   int id = Integer.parseInt(edtuid.getText().toString());
                String name = edtname.getText().toString();
                String pass = edtpass.getText().toString();

                TableModel tm = new TableModel();
                tm.setId(id);
                tm.setName(name);
                tm.setPass(pass);
                tm.setImei(IMEINumber);
                tm.setCareerprovider(NetworkProviderName);
                tm.save();*/

//                List<TableModel> users = new Select().from(TableModel.class).queryList();
//                StringBuilder builder = new StringBuilder();
//                for (TableModel person : users) {
//                    builder.append("Name: ")
//                            .append(person.name)
//                            .append(" UID: ")
//                            .append(person.id)
//                            .append("imei :")
//                            .append(person.imei)
//                            .append("coreer Pro :" + person.careerprovider)
//                            .append("\n");
//                }
//
//                Toast.makeText(getApplicationContext(), builder.toString(), Toast.LENGTH_LONG).show();

//                editor.putInt("Did",id);
//                editor.commit();

                if(CheckFieldValidation()){
                    int id = Integer.valueOf(edtuid.getText().toString());
                    String name = edtname.getText().toString();
                    String pass = edtpass.getText().toString();
                    String imei = mypref.getString(Constants.IMEI,"");
                    String operator = mypref.getString(Constants.Operator,"");
                    String firebaseid = mypref.getString(Constants.FirebaseID,"");
                    Signin(name,pass,id,imei,operator,firebaseid);
                }

//                Log.d("0000000000000","login");
//
//                Intent i = new Intent(getApplicationContext(), Drawer.class);
//                startActivity(i);
            }
        });
    }
//    private void insertUser(String username, String pass, int driverid){
//        //Here we will handle the http request to insert user to mysql db
//        //Creating a RestAdapter
//        RestAdapter adapter = new RestAdapter.Builder()
//                .setEndpoint(Login.BAS_URL) //Setting the Root URL
//                .build(); //Finally building the adapter
//
//        //Creating object for our interface
//        LoginInterface api = adapter.create(LoginInterface.class);
//
//        //Defining the method insertuser of our interface
//        api.Login(
//                username, pass, driverid, new Callback<LoginModel>() {
//                    @Override
//                    public void success(LoginModel loginModel, Response response) {
//
//                        finish();
//                        startActivity(getIntent());
//
//                        if (model.getStatus().equals("1")) {  //login Success
//
//                            Toast.makeText(Login.this, "Login In SuccessFully", Toast.LENGTH_SHORT).show();
//                            Intent i = new Intent(Login.this,Drawer.class);
//                            startActivity(i);
//
//                        } else if (model.getStatus().equals("0"))  // login failure
//                        {
//                            Toast.makeText(Login.this, "Invalid UserName/Pass ", Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//
//                    @Override
//                    public void failure(RetrofitError error) {
//
//                    }
//                }
//                //Creating an anonymous callback
////        new Callback<Response>() {
////                    @Override
////                    public void success(Response result, Response response) {
////                        //On success we will read the server's output using bufferedreader
////                        //Creating a bufferedreader object
////                        BufferedReader reader = null;
////
////                        //An string to store output from the server
////                        String output = "";
////
////                        try {
////                            //Initializing buffered reader
////                            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
////
////                            //Reading the output in the string
////                            output = reader.readLine();
////                        } catch (IOException e) {
////                            e.printStackTrace();
////                        }
////
////                        //Displaying the output as a toast
////                        Toast.makeText(Login.this, output, Toast.LENGTH_LONG).show();
////                    }
////
////                    @Override
////                    public void failure(RetrofitError error) {
////                        //If any error occured displaying the error as toast
////                        Toast.makeText(Login.this, error.toString(),Toast.LENGTH_LONG).show();
////                    }
////                }
//        );
//    }

    //checking field are empty
    private boolean CheckFieldValidation(){

        boolean valid=true;
        if(edtname.getText().toString().equals("")){
            edtname.setError("no puede estar vacía");
            valid=false;
        }else if(edtpass.getText().toString().equals("")){
            edtpass.setError("no puede estar vacía");
            valid=false;
        }else if (edtuid.getText().toString().equals("")){
            edtuid.setError("no puede estar vacía");
            valid=false;
        }
        return valid;
    }
    public void Signin(String username, String password, final int userid, final String imei, final String operator, final String firebaseid){
        Log.wtf("Transport", "Signin: "+username+password+userid );

        //Calling method of field validation
        if(CheckFieldValidation()) {


            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder();
// add your other interceptors …

// add logging as last interceptor
            httpClient.addInterceptor(logging);  // <-- this is the important line!
            final ProgressDialog loading = ProgressDialog.show(this,"Cargando","por favor espera...",false,false);
            HttpUrl u= HttpUrl.parse(Constants.url);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
//// TODO: 16/7/16 Modified This Request (Start)
            LoginInterface service = retrofit.create(LoginInterface.class);
            Call<LoginModel> call = service.GetLoginDetails(username, password, userid,imei,operator,firebaseid);
            call.enqueue(new Callback<LoginModel>() {
                @Override
                public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {

                    if (response != null) {
                        Log.wtf("Transport", "Response: "+response.message());

                        if (response.code()==200){

                            if (response.body().error.equals(false)) {  //login Success
                                if (response.body().match.equals(true)){
                                    Toast.makeText(Login.this, "En iniciar sesión con éxito", Toast.LENGTH_SHORT).show();
                                    mypref.putInt(Constants.KEY_DRIVER_ID, response.body().did);
                                    mypref.putInt(Constants.KEY_CUSTOMER_ID, response.body().cid);
                                    mypref.putString(Constants.KEY_API_KEY, response.body().apikey);
                                    mypref.putString(Constants.KEY_DRIVER_NAME, response.body().name);
                                    mypref.putString(Constants.KEY_DRIVER_IMAGE, response.body().image);

                                    // int vid = response.body().vid;
                                    // if (vid == 0){

                                    //  }else {
                                    mypref.putString(Constants.KEY_VEHICLE_ID, response.body().vid);
                                    mypref.putString(Constants.KEY_VEHICLE_IMAGE, response.body().vimage);
                                    mypref.putString(Constants.KEY_VEHICLE_BRAND, response.body().vbrand);
                                    mypref.putString(Constants.KEY_VEHICLE_MODEL, response.body().vmodel);
                                    mypref.putString(Constants.KEY_ADMIN_MAIL, response.body().adminmail);
                                    // }
                                    mypref.putInt(Constants.KEY_ISLOGIN,1);
                                    mypref.commit();

                                    Intent Serviceintent = new Intent(Login.this, MyLocationService.class);
                                    startService(Serviceintent);
                                    Intent i = new Intent(Login.this, Drawer.class);
                                    startActivity(i);
                                    Login.this.finish();
                                    loading.dismiss();
                                }else {
                                    loading.dismiss();
                                    //Toast.makeText(Login.this, "6animani Sachi Device Mathi Login Tha", Toast.LENGTH_LONG).show();
                                    alertAlertforIMEI("Solicitud de Nuevo dispositivo ?","Solicitud de administración",1,userid,imei,operator,firebaseid);
                                }

                            } else if(response.body().message.equals("AlreadyLogin"))// login failure
                            {
                                loading.dismiss();
                                Log.v("999999999999",response.body().toString());
                                Toast.makeText(Login.this, "Ya inicio de sesión en otro dispositivo", Toast.LENGTH_SHORT).show();

                            }else{
                                loading.dismiss();
                                Log.v("99999999999900",response.body().toString());
                                Toast.makeText(Login.this, "Nombre de usuario no válido/Contraseña o ID de conductor ", Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            loading.dismiss();
                           // Toast.makeText(Login.this, "Invalid Response From Server:"+response.code(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        loading.dismiss();
                        Toast.makeText(Login.this,response.body().toString() , Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<LoginModel> call, Throwable t) {
                    loading.dismiss();
                    Toast.makeText(Login.this,"Inténtalo de nuevo.", Toast.LENGTH_LONG).show();
                   // Log.wtf("Transport", "onFailure: "+t.getCause());
                }
            });
            //// TODO: 16/7/16 Modified This Request (end)
        }
    }

    public void loadIMEI() {
        // Check if the READ_PHONE_STATE permission is already available.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // READ_PHONE_STATE permission has not been granted.
            requestReadPhoneStatePermission();
        } else {
            // READ_PHONE_STATE permission is already been granted.
            doPermissionGrantedStuffs();
        }
    }
    private void requestReadPhoneStatePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_PHONE_STATE)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            new AlertDialog.Builder(Login.this)
                    .setTitle("Solicitud de permiso")
                    .setMessage("Request for getting id")
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //re-request
                            ActivityCompat.requestPermissions(Login.this,
                                    new String[]{Manifest.permission.READ_PHONE_STATE},
                                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
                        }
                    })
                    .setIcon(R.drawable.systemlogo)
                    .show();
        } else {
            // READ_PHONE_STATE permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == MY_PERMISSIONS_REQUEST_READ_PHONE_STATE) {
            // Received permission result for READ_PHONE_STATE permission.est.");
            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // READ_PHONE_STATE permission has been granted, proceed with displaying IMEI Number
                //alertAlert(getString(R.string.permision_available_read_phone_state));
                doPermissionGrantedStuffs();
            } else {
                alertAlert("El permiso no se concedió","Solicitud de permiso");
            }
        }
    }

    private void alertAlert(String msg, String title) {
        new AlertDialog.Builder(Login.this)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(R.drawable.systemlogo)
                .show();
    }
    private void alertAlertforIMEI(String msg, String title, final int id, final int d_id, final String imei, final String operator, final String fid) {
        new AlertDialog.Builder(Login.this)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (id == 1){
                            ReqIMEI(d_id,imei,operator,fid);
                           // Toast.makeText(Login.this, "Request Sended...", Toast.LENGTH_SHORT).show();
                        }else{
                            finish();
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setIcon(R.drawable.systemlogo)
                .show();
    }

    private boolean isRegistered() {
        return mypref.getBoolean(Constants.REGISTERED, false);
    }
    private void registerDevice() {
        //Creating a firebase object
        Firebase firebase = new Firebase(Constants.FIREBASE_APP);

        //Pushing a new element to firebase it will automatically create a unique id
        Firebase newFirebase = firebase.push();

        //Creating a map to store name value pair
        Map<String, String> val = new HashMap<>();

        //pushing msg = none in the map
        val.put("msg", "none");
        val.put("id","none");

        //saving the map to firebase
        newFirebase.setValue(val);

        //Getting the unique id generated at firebase
        String uniqueId = newFirebase.getKey();
        // Log.d("999999999999",uniqueId);
        mypref.putString(Constants.FirebaseID,uniqueId);
        mypref.putBoolean(Constants.REGISTERED,true);
        mypref.commit();
        String firebaseID  = mypref.getString(Constants.FirebaseID,"");
       // Toast.makeText(Login.this, "Firebase ID : " + firebaseID, Toast.LENGTH_SHORT).show();
        //Finally we need to implement a method to store this unique id to our server
        // sendIdToServer(uniqueId);
    }

    public void ReqIMEI(int userid, String imei, String operator, String firebaseid){
        //Calling method of field validation
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder();
        httpClient.addInterceptor(logging);  // <-- this is the important line!

        final ProgressDialog loading = ProgressDialog.show(this,"Cargando","por favor espera...",false,false);
        HttpUrl u= HttpUrl.parse(Constants.url);

        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Constants.url)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build();
        //// TODO: 16/7/16 Modified This Request (Start)
            SlimIface service = retrofit.create(SlimIface.class);
            Call<ReqIMEI> call = service.ReqIMEI(userid,imei,operator,firebaseid);
            call.enqueue(new Callback<ReqIMEI>() {
                @Override
                public void onResponse(Call<ReqIMEI> call, Response<ReqIMEI> response) {
                    if (response != null) {
                        Log.wtf("Transport", "Response: "+response.message());
                        if (response.code()==200){
                            if (response.body().error.equals(false)) {  //Req. Success
                                Toast.makeText(Login.this, response.body().message.toString(), Toast.LENGTH_SHORT).show();
                                loading.dismiss();
                                finish();
                            }else {
                                Toast.makeText(Login.this, response.body().message.toString(), Toast.LENGTH_SHORT).show();
                                loading.dismiss();
                                finish();
                            }
                        }else if(response.code()==201){
                            if (response.body().error.equals(true)) {
                                Toast.makeText(Login.this, response.body().message.toString(), Toast.LENGTH_SHORT).show();
                                loading.dismiss();
                                finish();
                            }
                        }
                    }
                    else {
                        loading.dismiss();
                        Toast.makeText(Login.this,response.body().toString() , Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<ReqIMEI> call, Throwable t) {
                    loading.dismiss();
                    Toast.makeText(Login.this,"Inténtalo de nuevo.", Toast.LENGTH_LONG).show();
                    // Log.wtf("Transport", "onFailure: "+t.getCause());
                }
        });
    }


    public void doPermissionGrantedStuffs() {
        //Have an  object of TelephonyManager
        TelephonyManager tm =(TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        //Get IMEI Number of Phone  //////////////// for this example i only need the IMEI
        IMEINumber=tm.getDeviceId();
        NetworkProviderName = tm.getNetworkOperatorName();
        phonenumber = tm.getLine1Number();
        String SIMSerialNumber=tm.getSimSerialNumber();
        mypref.putString(Constants.IMEI,IMEINumber);
        mypref.putString(Constants.Operator,NetworkProviderName);
        /************************************************
         * **********************************************
         * This is just an icing on the cake
         * the following are other children of TELEPHONY_SERVICE
         *
         //Get Subscriber ID
         String subscriberID=tm.getDeviceId();

         //Get SIM Serial Number
         String SIMSerialNumber=tm.getSimSerialNumber();

         //Get Network Country ISO Code
         String networkCountryISO=tm.getNetworkCountryIso();

         //Get SIM Country ISO Code
         String SIMCountryISO=tm.getSimCountryIso();

         //Get the device software version
         String softwareVersion=tm.getDeviceSoftwareVersion()

         //Get the Voice mail number
         String voiceMailNumber=tm.getVoiceMailNumber();


         //Get the Phone Type CDMA/GSM/NONE
         int phoneType=tm.getPhoneType();

         switch (phoneType)
         {
         case (TelephonyManager.PHONE_TYPE_CDMA):
         // your code
         break;
         case (TelephonyManager.PHONE_TYPE_GSM)
         // your code
         break;
         case (TelephonyManager.PHONE_TYPE_NONE):
         // your code
         break;
         }

         //Find whether the Phone is in Roaming, returns true if in roaming
         boolean isRoaming=tm.isNetworkRoaming();
         if(isRoaming)
         phoneDetails+="\nIs In Roaming : "+"YES";
         else
         phoneDetails+="\nIs In Roaming : "+"NO";


         //Get the SIM state
         int SIMState=tm.getSimState();
         switch(SIMState)
         {
         case TelephonyManager.SIM_STATE_ABSENT :
         // your code
         break;
         case TelephonyManager.SIM_STATE_NETWORK_LOCKED :
         // your code
         break;
         case TelephonyManager.SIM_STATE_PIN_REQUIRED :
         // your code
         break;
         case TelephonyManager.SIM_STATE_PUK_REQUIRED :
         // your code
         break;
         case TelephonyManager.SIM_STATE_READY :
         // your code
         break;
         case TelephonyManager.SIM_STATE_UNKNOWN :
         // your code
         break;

         }
         */
        // Now read the desired content to a textview.
        loading_tv2 = (TextView) findViewById(R.id.loading_tv2);
      //  loading_tv2.setText(IMEINumber + " Provider : " + NetworkProviderName + " Number : " + phonenumber + " 2 : " + SIMSerialNumber);
    }
}
