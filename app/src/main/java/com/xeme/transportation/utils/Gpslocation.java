package com.xeme.transportation.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.xeme.transportation.utils.Constants.LogIt;

public class Gpslocation implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {


    public static GoogleApiClient mGoogleApiClient;
    public static LocationRequest mLocationRequest;
    public static Location mLastLocation;
    public static Location mCurrentLocation;
    public static boolean isconnected = false;
    static Context mContext;


    public Gpslocation(Context m) {


        try {
            mContext = m;
            buildGoogleApiClient();
            mGoogleApiClient.connect();
            createLocationRequest();
        } catch (Exception e) {
            Log.d(Constants.TAG, e.toString());
        }

    }

    public static LatLng getlatlong() {

        try {

            if (mCurrentLocation != null) {

                return new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            }

            if (mLastLocation != null) {
                return new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());

            }
            return new LatLng(Constants.INITIAL_CENTER_LAT, Constants.INITIAL_CENTER_LNG);
        } catch (Exception e) {

            Log.wtf(Constants.TAG, "********Error******* :" + e);
            return new LatLng(Constants.INITIAL_CENTER_LAT, Constants.INITIAL_CENTER_LNG);
        }


    }

   /* public static void movetolastlocation(GoogleMap map) {

        try {

            if (map != null)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(getlatlong(), Constants.INITIAL_ZOOM_LEVEL));
        } catch (Exception e) {

            Log.wtf(Constants.TAG, "********Error******* :" + e);
        }

    }*/

    public static void getAddressFromLocation(
            final Location location, final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String result = null;
                String result2 = "";
                double lat = 0.0;
                double lng = 0.0;
                try {
                    List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

                    if (addresses != null && addresses.size() > 0) {

                        Address address = addresses.get(0);
                        ArrayList<String> addressFragments = new ArrayList<String>();

                        lat = address.getLatitude();
                        lng = address.getLongitude();

                        for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                            if (!address.getAddressLine(i).equals("null")) {
                                addressFragments.add(address.getAddressLine(i));
                                //Log.wtf(Constants.TAG,"line "+i+" "+address.getAddressLine(i));
                            }
                        }
                        // sending back first address line and locality

                        for (String obj : addressFragments) {

                            if (!obj.equals("null"))

                                result2 = result2 + (obj + ", ");
                        }

                        result = result2;
                        // result; /* = TextUtils.join(System.getProperty("line.separator"), addressFragments);*/
                    }
                } catch (IOException e) {
                    Log.wtf(Constants.TAG, "Impossible to connect to Geocoder", e);

                } finally {
                    Message msg = Message.obtain();
                    msg.setTarget(handler);
                    if (result != null) {
                        msg.what = 1;
                        Bundle bundle = new Bundle();
                        bundle.putString("address", result);
                        bundle.putDouble("lat", lat);
                        bundle.putDouble("lng", lng);
                        msg.setData(bundle);

                    } else
                        msg.what = 0;
                    msg.sendToTarget();
                }
            }
        };
        thread.start();
    }


    public static void getAddressFromLocation2(
            final Location location, final Context context, final Handler handler, final String forwho) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String result = null;
                String result2 = "";
                double lat = 0.0;
                double lng = 0.0;
                try {
                    List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

                    if (addresses != null && addresses.size() > 0) {

                        Address address = addresses.get(0);
                        ArrayList<String> addressFragments = new ArrayList<String>();

                        lat = address.getLatitude();
                        lng = address.getLongitude();

                        for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                            if (!address.getAddressLine(i).equals("null")) {
                                addressFragments.add(address.getAddressLine(i));
                                //Log.wtf(Constants.TAG,"line "+i+" "+address.getAddressLine(i));
                            }
                        }
                        // sending back first address line and locality

                        for (String obj : addressFragments) {

                            if (!obj.equals("null"))

                                result2 = result2 + (obj + ", ");
                        }

                        result = result2;
                        // result; /* = TextUtils.join(System.getProperty("line.separator"), addressFragments);*/
                    }
                } catch (IOException e) {
                    Log.wtf(Constants.TAG, "Impossible to connect to Geocoder", e);

                } finally {
                    Message msg = Message.obtain();
                    msg.setTarget(handler);
                    if (result != null) {
                        msg.what = 1;
                        Bundle bundle = new Bundle();
                        bundle.putString("address", result);
                        bundle.putDouble("lat", lat);
                        bundle.putDouble("lng", lng);
                        bundle.putString("forwho", forwho);
                        msg.setData(bundle);

                    } else
                        msg.what = 0;
                    msg.sendToTarget();
                }
            }
        };
        thread.start();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        try {
            isconnected = true;
            startLocationUpdates();

            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                // Determine whether a Geocoder is available.
                if (!Geocoder.isPresent()) {

                    return;
                }


            }
        } catch (SecurityException s) {

            Log.wtf(Constants.TAG, "********Error******* :" + s);
        } catch (Exception x) {
            Log.wtf(Constants.TAG, "********Error******* :" + x);
        }


    }

    private void startLocationUpdates() {


        try {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        mGoogleApiClient, mLocationRequest, this);
            }
        } catch (SecurityException s) {

            Log.wtf(Constants.TAG, "********Error******* :" + s);
        } catch (Exception e) {
            Log.wtf(Constants.TAG, "********Error******* :" + e);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        isconnected = false;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onLocationChanged(Location location) {

        //obdemo.setlocation(location);
        // myClass.notifySomethingHappened();
        mCurrentLocation = location;
       LogIt("onLocationChanged gpsloaction",location.toString());

        //   MmyLocation.MyLocationChanged(new LatLng(Constants.INITIAL_CENTER.latitude, Constants.INITIAL_CENTER.longitude));
        //myCallbacks.setMyLocation(new LatLng(Constants.INITIAL_CENTER.latitude, Constants.INITIAL_CENTER.longitude));

    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(Constants.UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(Constants.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

}
