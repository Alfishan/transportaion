package com.xeme.transportation.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.xeme.transportation.R;

import java.util.Calendar;

public class MyLocationService extends Service implements LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    // Gpslocation gpslocation=new Gpslocation(MyLocationService.this);


    public static GoogleApiClient mGoogleApiClient;
    public static LocationRequest mLocationRequest;
    public static Location mLastLocation;
    public static Location mCurrentLocation;
    public static boolean isconnected = false;
    static Context mContext;
    Calendar mCalendar;
    int did, cid;
    DatabaseReference databaseReference;
    MySharedPreferences mypref;
    int NotifID =25747426;
    NotificationManager notificationManager;

    public MyLocationService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.wtf(Constants.TAG, "Service onStartCommand" + NotifID);
        mContext = MyLocationService.this;
        buildGoogleApiClient();
        mGoogleApiClient.connect();
        createLocationRequest();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        CreateNotification();


//        SharedPreferences pref = mContext.getSharedPreferences("MyDid", 0); // 0 - for private mode
//        did = pref.getInt("Did", 0);
        mypref = MySharedPreferences.getInstance(mContext, "Transportation");
        did = mypref.getInt(Constants.KEY_DRIVER_ID, 0);
        cid = mypref.getInt(Constants.KEY_CUSTOMER_ID, 0);
        System.out.println("PREF" + did);

        databaseReference = FirebaseDatabase.getInstance().getReference();

        mCalendar = Calendar.getInstance();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        notificationManager.cancel(NotifID);
        Log.wtf(Constants.TAG, "Notif Destroyed" + NotifID);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.wtf(Constants.TAG, "Service Created" + NotifID);

    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        databaseReference.child("driver").child("location").child(String.valueOf(cid)).child(String.valueOf(did)).setValue(location);
        //MyLocation myLocation=new MyLocation(getLat(),getLng(),getTime());
        // myLocation.save();
        // Constants.LogIt("onLocationChanged of Services",location.toString());
    }

    private void startLocationUpdates() {


        try {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        mGoogleApiClient, mLocationRequest, this);
            }
        } catch (SecurityException s) {

            Log.wtf(Constants.TAG, "********Error******* :" + s);
        } catch (Exception e) {
            Log.wtf(Constants.TAG, "********Error******* :" + e);
        }

    }

    private void stopLocationUpdates() {
        try {

            Gpslocation.mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    Gpslocation.mGoogleApiClient);

            if (Gpslocation.mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        Gpslocation.mGoogleApiClient, this);
            }
        } catch (SecurityException e) {

            e.printStackTrace();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        try {
            isconnected = true;
            startLocationUpdates();

            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                // Determine whether a Geocoder is available.
                if (!Geocoder.isPresent()) {

                    return;
                }


            }
        } catch (SecurityException s) {

            Log.wtf(Constants.TAG, "********Error******* :" + s);
        } catch (Exception x) {
            Log.wtf(Constants.TAG, "********Error******* :" + x);
        }


    }

    @Override
    public void onConnectionSuspended(int i) {
        isconnected = false;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(Constants.UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(Constants.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    long getTime() {

        if (mCalendar != null) {

            return mCalendar.getTimeInMillis();
        } else {
            mCalendar = Calendar.getInstance();
            return mCalendar.getTimeInMillis();

        }

    }


    public double getLat() {

        return mCurrentLocation.getLatitude();
    }

    public double getLng() {

        return mCurrentLocation.getLongitude();
    }

    void CreateNotification() {
       // Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.systemlogo)
                .setContentTitle("FSG GRIP")
                .setContentText("Aplicación se está ejecutando")
                .setAutoCancel(false).setPriority(1)
                .setOngoing(true); //.setSound(defaultSoundUri)

        Notification myNotif = notificationBuilder.build();
        myNotif.flags = Notification.FLAG_ONGOING_EVENT;
        notificationManager.notify(NotifID, myNotif);
        Log.wtf(Constants.TAG, "Notif Created" + NotifID);
    }

}
