package com.xeme.transportation.utils;


import android.util.Log;

/**
 * Constant values reused in this sample.
 */
public final class Constants {

    public static String url ="https://miflotilla.online";// "http://fsg.grip.com.mx";

    public static final int SUCCESS_RESULT = 0;

    public static final int FAILURE_RESULT = 1;

    public static final String PACKAGE_NAME = "com.xeme.transportation";


    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";
    public static final int Permission_Fine_Location = 123;
    public static final int Permission_Write_external = 124;
    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 125;
    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 60000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    public static final String TAG = "Transportation";

    public static final int INITIAL_DELAY_MILLIS = 300;


  //  public static final Location INITIAL_CENTER = new Location(22.9936, 72.5056);

    public static final Double INITIAL_CENTER_LAT = 22.9936;
    public static final Double INITIAL_CENTER_LNG = 72.5056;

    public static final String IsTripStarted="IsTripStarted";
    public static final String KEY_DRIVER_ID="DRIVER_ID";
    public static final String KEY_CUSTOMER_ID="CUSTOMER_ID";
    public static final String KEY_API_KEY="API_KEY";
    public static final String KEY_DRIVER_NAME="DRIVER_NAME";
    public static final String KEY_DRIVER_IMAGE="KEY_DRIVER_IMAGE";
    public static final String KEY_ADMIN_MAIL = "Admin_Mail";

    public static final String KEY_ISLOGIN = "is_Login";

    // for Driver trip.
    public static final String KEY_TRIP_ID = "TRIP_ID";
    public static final String KEY_VEHICLE_ID = "VEHICLE_ID";
    public static final String KEY_VEHICLE_IMAGE = "";
    public static final String KEY_VEHICLE_BRAND = "VEHICLE_BRAND";
    public static final String KEY_VEHICLE_MODEL = "VEHICLE_MODEL";
    public static final String KEY_STARTTRIP = "STARTTRIP";
    public static final String KEY_ENDTRIP = "ENDTRIP";

    public static final String IMEI = "IMEI";
    public static final String Operator = "operator";
    public static final String FIREBASE_APP = "https://grip-fsg.firebaseio.com/sms/";
    public static final String REGISTERED = "registered";
    public static final String FirebaseID = "firebaseid";
    public static final String FirebaseMsg = "firebasemsg";

    public static final String MAPSTARTLAT = "startlat";
    public static final String MAPSTARTLONG = "startlong";
   // public static final String MAPSTARTLANG = "startlang";
   // public static final String MAPStopLAT = "stoplat";
    public static final String MAPStopLAt = "stoplat";
    public static final String MAPStopLong = "stoplong";
    public static final Double latlang = 0.0;
    public static final Double latlangdesti = 0.0;
    public static void LogIt(String From, String msg){

        Log.wtf("MyLogs","*x*x*x*x*x "+From+":="+msg);
    }

}
