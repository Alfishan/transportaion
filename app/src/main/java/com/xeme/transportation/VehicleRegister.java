package com.xeme.transportation;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class VehicleRegister extends AppCompatActivity {
    String[] type = {"Car","Truck","Bus"};
    String[] manufacturer = {"BMW","Ashok Layland","Volvo"};
    String[] fuel = {"Ethanol","Deasel","Gas"};

    Spinner vehicletype,manufacture,vehiclefuel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_register);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);

        vehicletype = (Spinner)findViewById(R.id.Vehicle);
        manufacture = (Spinner)findViewById(R.id.VehicleManufacturer);
        vehiclefuel = (Spinner)findViewById(R.id.VihecleFual);

        ArrayAdapter<String> adaptertype = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item,type);
        vehicletype.setAdapter(adaptertype);

        ArrayAdapter<String> adaptermanu = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, manufacturer);
        manufacture.setAdapter(adaptermanu);

        ArrayAdapter<String> adapterfuel = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, fuel);
        vehiclefuel.setAdapter(adapterfuel);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }
}
