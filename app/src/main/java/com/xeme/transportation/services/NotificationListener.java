package com.xeme.transportation.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.xeme.transportation.NotiMessage;
import com.xeme.transportation.R;
import com.xeme.transportation.SplashScreen;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.MySharedPreferences;

/**
 * Created by root on 27/7/16.
 */
public class NotificationListener extends Service {

    MySharedPreferences mypref;
    String vehicleremove = "3";
    String vehicleid,vehiclebrand,msg,vehiclemodel,startloc,endloc,vehicleimage;
    String fid= "1",FMsg,title = "Message From Grip";


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //When the service is started
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Opening sharedpreferences
        mypref = MySharedPreferences.getInstance(this,"Transportation");
       // SharedPreferences sharedPreferences = getSharedPreferences("mypref", MODE_PRIVATE);

        //Getting the firebase id from sharedpreferences
        String id = mypref.getString(Constants.FirebaseID, null);
        FMsg = mypref.getString(Constants.FirebaseMsg,null);

      //  Toast.makeText(NotificationListener.this, "firebaseID : " + id, Toast.LENGTH_SHORT).show();
      //  Log.d("9999999999999999","this is service");

        //Creating a firebase object
        Firebase firebase = new Firebase(Constants.FIREBASE_APP +  id);
      //  Log.d("9999999999999999","" + firebase);
        //Adding a valueevent listener to firebase
        //this will help us to  track the value changes on firebase
        firebase.addValueEventListener(new ValueEventListener() {

            //This method is called whenever we change the value in firebase
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //Getting the value from firebase
                //We stored none as a initial value
                fid = snapshot.child("id").getValue().toString();
                msg = snapshot.child("msg").getValue().toString();
//                vehicleid = snapshot.child("vehicleid").getValue().toString();
//                vehiclebrand = snapshot.child("brandname").getValue().toString();
//                vehiclemodel = snapshot.child("vehiclemodel").getValue().toString();
//                startloc = snapshot.child("start").getValue().toString();
//                endloc = snapshot.child("end").getValue().toString();
                //So if the value is none we will not create any notification
                if (msg.equals("none")) {
                    return;
                }
                else if (msg.equals(FMsg)) {
                    return;
            }
                else if (fid.equals("2")) {

                    vehicleid = snapshot.child("vehicleid").getValue().toString();
                    vehiclebrand = snapshot.child("brandname").getValue().toString();
                    System.out.println("VImage url " + snapshot.child("vehicleimage").getValue().toString());
                    vehicleimage = snapshot.child("vehicleimage").getValue().toString();
                    vehiclemodel = snapshot.child("vehiclemodel").getValue().toString();

                    mypref.putString(Constants.FirebaseMsg, msg);
                    mypref.putString(Constants.KEY_VEHICLE_ID,vehicleid);
                    mypref.putString(Constants.KEY_VEHICLE_BRAND,vehiclebrand);
                    mypref.putString(Constants.KEY_VEHICLE_MODEL,vehiclemodel);
                    mypref.putString(Constants.KEY_VEHICLE_IMAGE, vehicleimage);
                    mypref.commit();
                    title = "You have Asign Vehicle";
                    showNotification("Assign Vehicle : " + vehiclebrand + ", "+ vehiclemodel, SplashScreen.class);

                }else if (msg.equals(vehicleremove) && fid.equals("3")){
                    vehicleid = snapshot.child("vehicleid").getValue().toString();
                    vehiclebrand = snapshot.child("brandname").getValue().toString();
                    vehiclemodel = snapshot.child("vehiclemodel").getValue().toString();
                    mypref.putString(Constants.FirebaseMsg, msg);
                    mypref.removeValue(Constants.KEY_VEHICLE_ID);
                    mypref.removeValue(Constants.KEY_VEHICLE_BRAND);
                    mypref.removeValue(Constants.KEY_VEHICLE_MODEL);
                    mypref.removeValue(Constants.KEY_VEHICLE_IMAGE);
                    mypref.commit();
                    title = "Vihicle was Removed";
                    showNotification("Your Assign Vihicle was Removed", SplashScreen.class);

                }else if (fid.equals("4")){
                    startloc = snapshot.child("start").getValue().toString();
                    endloc = snapshot.child("end").getValue().toString();
                    mypref.putString(Constants.FirebaseMsg, msg);
                    mypref.putString(Constants.KEY_STARTTRIP, startloc);
                    mypref.putString(Constants.KEY_ENDTRIP, endloc);
                    title = "You Have New Trip";
                    showNotification("You have new Trip Assigned",NotiMessage.class);

                }
                else {
                    mypref.putString(Constants.FirebaseMsg, msg);
                    showNotification(msg,NotiMessage.class);
                }
                //If the value is anything other than none that means a notification has arrived
                //calling the method to show notification
                //String msg is containing the msg that has to be shown with the notification

            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e("The read failed: ", firebaseError.getMessage());
            }
        });

        return START_STICKY;
    }

    private void showNotification(String msg, Class open) {
        Log.d("Openclass : ", String.valueOf(open));
        Intent i = new Intent(this, open);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int requestCode = 0;
        PendingIntent pendingi = PendingIntent.getActivity(this, requestCode, i, PendingIntent.FLAG_ONE_SHOT);
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder noBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(msg)
                .setSound(sound)
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                .setContentTitle(title)
                .setAutoCancel(true)
                .setContentIntent(pendingi);

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, noBuilder.build()); //0 = ID of notification
    }
}