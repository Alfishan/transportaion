package com.xeme.transportation.Fragment;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.xeme.transportation.Adapter.StarttripRecordsAdapter;
import com.xeme.transportation.Bean.ExRowItem;
import com.xeme.transportation.Interface.SlimIface;
import com.xeme.transportation.POJO.Starttripdata;
import com.xeme.transportation.POJO.Starttripdataresponse;
import com.xeme.transportation.R;
import com.xeme.transportation.StartTrip;
import com.xeme.transportation.StopTrip;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.MySharedPreferences;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by root on 24/5/16.
 */
public class Frag_StartTripRecords extends Fragment {
    //    FragmentManager fm = getFragmentManager();
//    LinearLayout linearLayoutrecords,linearLayoutframe;

    ListView liststarttrip;
    List<ExRowItem> exrowItems;
    Integer toggle;
    MySharedPreferences mypref;
    int driverid;

    public Frag_StartTripRecords() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_starttriprecords, null);

        SharedPreferences pref = getActivity().getSharedPreferences("MyPref", 0); // 0 - for private mode
        toggle = pref.getInt("Toggle", 0);
        mypref = MySharedPreferences.getInstance(getActivity(),"Transportation");
        driverid = mypref.getInt(Constants.KEY_DRIVER_ID,0);

        liststarttrip = (ListView)root.findViewById(R.id.liststartrecords);

        GetStarttripData();

        FloatingActionButton fab = (FloatingActionButton) root.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (toggle != 1){
                    Intent i = new Intent(getActivity(), StartTrip.class);
                   // i.putExtra("isfromnoti",1);
                    startActivity(i);
                }else {
                    Intent i = new Intent(getActivity(), StopTrip.class);
                    startActivity(i);
                }
            }
        });

        return root;
    }
    @Override
    public void onResume() {
        super.onResume();
        GetStarttripData();
    }

    public void GetStarttripData(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create())
                // .client(httpClient.build())
                .build();

        SlimIface service = retrofit.create(SlimIface.class);
        System.out.println("0000000000" + driverid);
        Call<Starttripdataresponse> call = service.getStartdata(driverid);
        System.out.println("1111111111" + call.toString());
        call.enqueue(new Callback<Starttripdataresponse>() {
            @Override
            public void onResponse(Call<Starttripdataresponse> call, Response<Starttripdataresponse> response) {

                if (response != null){
                    Log.wtf("2222222222222", "Response: "+response.message());
                    if (response.body().error.equals(false)) {
                        System.out.println("2222222222" + response.toString());
                        List<Starttripdata> refuelingdatas = response.body().getResults();
                        System.out.println("3333333333 "+refuelingdatas.toString());

                       // liststarttrip.setAdapter(new StarttripRecordsAdapter(getActivity(),R.layout.liststarttriprecords,refuelingdatas));
                        StarttripRecordsAdapter adapter = new StarttripRecordsAdapter(getActivity(), R.layout.liststarttriprecords, refuelingdatas);

                        SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(adapter);
                        swingBottomInAnimationAdapter.setAbsListView(liststarttrip);

                        assert swingBottomInAnimationAdapter.getViewAnimator() != null;
                        swingBottomInAnimationAdapter.getViewAnimator().setInitialDelayMillis(
                                Constants.INITIAL_DELAY_MILLIS);

                        liststarttrip.setAdapter(swingBottomInAnimationAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<Starttripdataresponse> call, Throwable t) {
                String merror = t.getMessage();
                Toast.makeText(getActivity(), merror, Toast.LENGTH_LONG).show();
                Log.wtf("Transport", "onFailure: "+t.getCause());
            }
        });
    }
}
