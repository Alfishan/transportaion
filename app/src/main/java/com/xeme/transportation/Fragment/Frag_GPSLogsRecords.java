package com.xeme.transportation.Fragment;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.xeme.transportation.Interface.SlimIface;
import com.xeme.transportation.POJO.LogLatLongresponse;
import com.xeme.transportation.POJO.LogsLatLongdata;
import com.xeme.transportation.R;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.GMapV2Direction;
import com.xeme.transportation.utils.Gpslocation;
import com.xeme.transportation.utils.MySharedPreferences;

import org.w3c.dom.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Frag_GPSLogsRecords extends Fragment implements OnMapReadyCallback, LocationListener {

    // TODO: 16/7/16  Defined for marker
    public static final int MARKER_REFUEL = 0;
    public static final int MARKER_SERVICE = 1;
    public static final int MARKER_EXPENSE = 2;
    private static View view;
    GoogleMap googleMap;
    Gpslocation gpslocation;

   MySharedPreferences mypref;
    String startLoc = "hii",endLoc = "hii";
    int tripid,driverid;

    // TODO: 16/7/16  Static Locations
    LatLng msource;
    LatLng mdestination;
    List<LogsLatLongdata> mListLogs;

    public Frag_GPSLogsRecords() {
        // Required empty public constructor
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_gpsrecords, null);
        } catch (InflateException e) {
        /* googleMap is already there, just return view as it is */
        }

        mypref = MySharedPreferences.getInstance(getActivity(), "Transportation");
        tripid = mypref.getInt(Constants.KEY_TRIP_ID,0);
        driverid = mypref.getInt(Constants.KEY_DRIVER_ID,0);

        if (tripid != 0){
            msource = new LatLng(mypref.getDouble(Constants.MAPSTARTLAT,0),mypref.getDouble(Constants.MAPSTARTLONG,0));
            mdestination = new LatLng(mypref.getDouble(Constants.MAPStopLAt,0),mypref.getDouble(Constants.MAPStopLong,0));
        }else {
            Toast.makeText(getActivity(), "Por favor Iniciar nuevo viaje", Toast.LENGTH_SHORT).show();
        }



       // ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);

        //SupportMapFragment supportMapFragment;
        if (Build.VERSION.SDK_INT < 21) {
            ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
        } else {
            ((MapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
        }
       // supportMapFragment.getMapAsync(this);

        gpslocation = new Gpslocation(getActivity());

        return view;
    }

    public void onLocationChanged(Location location) {

        LatLng latlng = new LatLng(location.getLatitude(), location.getLongitude());// This methods gets the users current longitude and latitude.

        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));//Moves the camera to users current longitude and latitude
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, (float) 14.6));//Animates camera and zooms to preferred state on the user's current location.
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        startLocationUpdates();
        try {
            googleMap.setMyLocationEnabled(true);
        } catch (SecurityException s) {

        }
        // TODO: 16/7/16  To add Path in Map
        //DrawRouteOnMap();
        GetLogsLatLong();

    }

    private void startLocationUpdates() {
        try {
            Gpslocation.mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    Gpslocation.mGoogleApiClient);

            if (Gpslocation.mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        Gpslocation.mGoogleApiClient, Gpslocation.mLocationRequest, this);
            }
        } catch (SecurityException e) {

            e.printStackTrace();
        }

    }


    // TODO: 16/7/16  To add Path in Map
    void DrawRouteOnMap() {


        if (msource != null && mdestination != null) {

            new LongOperation().execute("");
        } else {
           // Toast.makeText(getActivity(), "Empty Origin and Destination Location", Toast.LENGTH_SHORT).show();
        }

    }
 // TODO: 16/7/16  To add marker
    void AddMarker(int type, LatLng location,String dateTime) {

        if (googleMap != null&&location!=null) {
            switch (type) {
                case MARKER_REFUEL:
                    googleMap.addMarker(new MarkerOptions()
                            .position(location)
                            .title("Refueling : "+dateTime).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_local_gas_station_black)));

                    break;
                case MARKER_SERVICE:

                    googleMap.addMarker(new MarkerOptions()
                            .position(location)
                            .title("Service : "+dateTime).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_build_black)));
                    break;
                case MARKER_EXPENSE:

                    googleMap.addMarker(new MarkerOptions()
                            .position(location)
                            .title("Expense : "+dateTime).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_monetization_on_black)));

                    break;

            }
        }
        else {
            Log.wtf(Constants.TAG, "********Error******* : AddMarker ");

        }




    }



    // TODO: 16/7/16  To add Path in map
    private class LongOperation extends AsyncTask<String, Void, PolylineOptions> {

        private PolylineOptions getDirection() {
            try {

                GMapV2Direction md = new GMapV2Direction();

                Document doc = md.getDocument(msource, mdestination,
                        GMapV2Direction.MODE_DRIVING);


                ArrayList<LatLng> directionPoint = md.getDirection(doc);
                PolylineOptions rectLine = new PolylineOptions().width(12).color(
                        Color.parseColor("#2196F3")).geodesic(true);

                for (int i = 0; i < directionPoint.size(); i++) {
                    rectLine.add(directionPoint.get(i));
                }


                return rectLine;
            } catch (Exception e) {
                ///possible error:
                ///java.lang.IllegalStateException: Error using newLatLngBounds(LatLngBounds, int): googleMap size can't be 0. Most likely, layout has not yet occured for the googleMap view.  Either wait until layout has occurred or use newLatLngBounds(LatLngBounds, int, int, int) which allows you to specify the googleMap's dimensions.
                return null;
            }

        }

        @Override
        protected PolylineOptions doInBackground(String... params) {
            PolylineOptions polylineOptions = null;
            try {
                polylineOptions = getDirection();
            } catch (Exception e) {
                Thread.interrupted();
            }
            return polylineOptions;
        }

        @Override
        protected void onPostExecute(PolylineOptions result) {
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you


            if (result != null) {
                try {
                    googleMap.clear();///TODO: clean the path only..

                    googleMap.addMarker(new MarkerOptions()
                            .position(mdestination)
                            .title("Your Destination Location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                    googleMap.addMarker(new MarkerOptions()
                            .position(msource)
                            .title("Your Origin Location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));


                    if (mListLogs != null) {
                        for (LogsLatLongdata listLog : mListLogs) {

                            AddMarker(Integer.valueOf(listLog.category),new LatLng(Double.valueOf(listLog.latitude),Double.valueOf(listLog.longitude)),listLog.dateTime);

                        }
                    }
 /*
// TODO: 16/7/16  To add marker bye its type
                    AddMarker(MARKER_REFUEL,result.getPoints().get(result.getPoints().size() / 2));
                    AddMarker(MARKER_REFUEL,result.getPoints().get(result.getPoints().size() - 400));
                    AddMarker(MARKER_SERVICE,result.getPoints().get(1000));
                    AddMarker(MARKER_EXPENSE,result.getPoints().get(2000));

                    googleMap.addMarker(new MarkerOptions()
                            .position(result.getPoints().get(result.getPoints().size() / 2))
                            .title("Refueling").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_local_gas_station_black)));

                    googleMap.addMarker(new MarkerOptions()
                            .position(result.getPoints().get(1000))
                            .title("Minor Repairing ").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_build_black)));
                    googleMap.addMarker(new MarkerOptions()
                            .position(result.getPoints().get(2000))
                            .title("Food Expense  ").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_monetization_on_black)));

                    googleMap.addMarker(new MarkerOptions()
                            .position(result.getPoints()
                                    .get(result.getPoints().size() - 400))
                            .title("Refueling")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_local_gas_station_black)));
*/
                /*Static markers*/
                    Marker s = googleMap.addMarker(new MarkerOptions()
                            .position(result.getPoints().get(0))
                            .title("Your Journey Start From Here")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

                    Marker d = googleMap.addMarker(new MarkerOptions()
                            .position(result.getPoints()
                                    .get(result.getPoints().size() - 1))
                            .title("Your Journey End Here")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                    LatLngBounds.Builder bc = new LatLngBounds.Builder();
                    bc.include(s.getPosition());
                    bc.include(d.getPosition());
                    //googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 100));
                    googleMap.addPolyline(result);
                } catch (Exception e) {

                    Log.wtf(Constants.TAG, "********Error******* :" + e);


                }
            } else {
                Log.wtf(Constants.TAG, "********Error******* : Null Result Response ");

            }


        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public void GetLogsLatLong(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create())
                // .client(httpClient.build())
                .build();

        SlimIface service = retrofit.create(SlimIface.class);
        System.out.println("0000000000" + driverid);
        Call<LogLatLongresponse> call = service.GetLogsLatLong(driverid);
        System.out.println("1111111111" + call.toString());
        call.enqueue(new Callback<LogLatLongresponse>() {
            @Override
            public void onResponse(Call<LogLatLongresponse> call, Response<LogLatLongresponse> response) {

                if (response != null){
                    Log.wtf("2222222222222", "Response: "+response.message());
                    if (response.body().error.equals(false)) {
//                        startLoc =  response.body().startloc;
//                        msource = getLocationFromAddress(startLoc);
//                        endLoc = response.body().endloc;
//                        mdestination = getLocationFromAddress(endLoc);
                        mListLogs=response.body().getResults();
                        DrawRouteOnMap();
                    }
                }
            }

            @Override
            public void onFailure(Call<LogLatLongresponse> call, Throwable t) {
                String merror = t.getMessage();
                Toast.makeText(getActivity(), merror, Toast.LENGTH_LONG).show();
                Log.wtf("Transport", "onFailure: "+t.getCause());
            }
        });
    }

    public LatLng getLocationFromAddress(String strAddress){

        Geocoder coder = new Geocoder(getActivity());
        List<Address> address;
        Geocoder p1 = null;
        LatLng p2 = null;

        try {
            address = coder.getFromLocationName(strAddress,5);
            if (address==null) {
                return null;
            }
            Address location=address.get(0);
            location.getLatitude();
            location.getLongitude();

           p2 = new LatLng(location.getLatitude(),location.getLongitude());

        }catch (IOException e) {
            e.printStackTrace();
        }
        return p2;
    }


}
