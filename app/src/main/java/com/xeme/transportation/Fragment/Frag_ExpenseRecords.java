package com.xeme.transportation.Fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.xeme.transportation.Adapter.ExpensedataAdapter;
import com.xeme.transportation.Expense;
import com.xeme.transportation.Interface.SlimIface;
import com.xeme.transportation.POJO.Expensedata;
import com.xeme.transportation.POJO.Expensedataresponse;
import com.xeme.transportation.R;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.MySharedPreferences;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by root on 23/5/16.
 */
public class Frag_ExpenseRecords extends Fragment {

//    FragmentManager fm = getFragmentManager();
//    LinearLayout linearLayoutrecords,linearLayoutframe;
    ListView listexpense;
    //private List<Refuelingdata> refuel;

   // List<ExRowItem> exrowItems;
    MySharedPreferences mypref;
    int tripID = 0,driverid;

//    public static final String[] expensetype = new String[] {"Car Wash","Fine"};
//    public static final String[] odometer = new String[] {"6555","7800"};
//    public static final String[] date = new String[] {"20 may","23 may"};
//    public static final String[] totalprice = new String[] {"1000","500"};

    public Frag_ExpenseRecords() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_expenserecords, null);
        mypref = MySharedPreferences.getInstance(getActivity(), "Transportation");
        tripID = mypref.getInt(Constants.KEY_TRIP_ID,0);
        driverid = mypref.getInt(Constants.KEY_DRIVER_ID,0);

        listexpense = (ListView)root.findViewById(R.id.listexpenserecords);
        GetExpensedata();
//        exrowItems = new ArrayList<ExRowItem>();
//        for (int i = 0; i < expensetype.length; i++) {
//            ExRowItem item = new ExRowItem(expensetype[i], odometer[i],date[i], totalprice[i]);
//            exrowItems.add(item);
//        }

//        StarttripRecordsAdapter adapter = new StarttripRecordsAdapter(getActivity(), R.layout.liststarttriprecords, exrowItems);
//        listexpense.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) root.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tripID == 0){
                    Toast.makeText(getActivity(), "En primer lugar usted necesita para empezar Nuevo viaje", Toast.LENGTH_SHORT).show();
                }else {
                    Intent i = new Intent(getActivity(), Expense.class);
                    startActivity(i);
                }
//                linearLayoutrecords.setVisibility(View.INVISIBLE);
//                linearLayoutframe.setVisibility(View.VISIBLE);

            }
        });

        return root;
    }
    @Override
    public void onResume() {
        super.onResume();
        GetExpensedata();
    }

    public void GetExpensedata(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create())
                // .client(httpClient.build())
                .build();

        SlimIface service = retrofit.create(SlimIface.class);
        System.out.println("0000000000" + driverid);
        Call<Expensedataresponse> call = service.getExpensedata(driverid);
        System.out.println("1111111111" + call.toString());
        call.enqueue(new Callback<Expensedataresponse>() {
            @Override
            public void onResponse(Call<Expensedataresponse> call, Response<Expensedataresponse> response) {

                if (response != null){
                    Log.wtf("2222222222222", "Response: "+response.message());
                    if (response.body().error.equals(false)) {
                        System.out.println("2222222222" + response.toString());
                        List<Expensedata> expesnsedatas = response.body().getResults();
                        System.out.println("3333333333 "+expesnsedatas.toString());

                        // liststarttrip.setAdapter(new StarttripRecordsAdapter(getActivity(),R.layout.liststarttriprecords,refuelingdatas));
                        ExpensedataAdapter adapter = new ExpensedataAdapter(getActivity(), R.layout.listexpenserecords, expesnsedatas);

                        SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(adapter);
                        swingBottomInAnimationAdapter.setAbsListView(listexpense);

                        assert swingBottomInAnimationAdapter.getViewAnimator() != null;
                        swingBottomInAnimationAdapter.getViewAnimator().setInitialDelayMillis(
                                Constants.INITIAL_DELAY_MILLIS);

                        listexpense.setAdapter(swingBottomInAnimationAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<Expensedataresponse> call, Throwable t) {
                String merror = t.getMessage();
                Toast.makeText(getActivity(), merror, Toast.LENGTH_LONG).show();
                Log.wtf("Transport", "onFailure: "+t.getCause());
            }
        });
    }
}
