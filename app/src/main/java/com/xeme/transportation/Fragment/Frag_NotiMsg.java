package com.xeme.transportation.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.xeme.transportation.Adapter.smsAdapter;
import com.xeme.transportation.Interface.SlimIface;
import com.xeme.transportation.POJO.smsdata;
import com.xeme.transportation.POJO.smsdataresponse;
import com.xeme.transportation.R;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.MySharedPreferences;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by root on 3/8/16.
 */
public class Frag_NotiMsg extends Fragment {

    ListView listmsg;
    MySharedPreferences mypref;
    int driverid;

    public Frag_NotiMsg() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_notimsg, null);
        mypref = MySharedPreferences.getInstance(getActivity(), "Transportation");
        driverid = mypref.getInt(Constants.KEY_DRIVER_ID,0);

        listmsg = (ListView)root.findViewById(R.id.listmsg);

        GetSMSData();
       // tripID = mypref.getInt(Constants.KEY_TRIP_ID,0);


        return root;
    }
    public void GetSMSData(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create())
                // .client(httpClient.build())
                .build();

        SlimIface service = retrofit.create(SlimIface.class);
        System.out.println("0000000000" + driverid);
        Call<smsdataresponse> call = service.getsmsdata(driverid);
        System.out.println("1111111111" + call.toString());
        call.enqueue(new Callback<smsdataresponse>() {
            @Override
            public void onResponse(Call<smsdataresponse> call, Response<smsdataresponse> response) {

                if (response != null){
                    Log.wtf("2222222222222", "Response: "+response.message());
                    if (response.body().error.equals(false)) {
                        System.out.println("2222222222" + response.toString());
                        List<smsdata> smsdatas = response.body().getResults();
                        System.out.println("3333333333 "+smsdatas.toString());

                        // liststarttrip.setAdapter(new StarttripRecordsAdapter(getActivity(),R.layout.liststarttriprecords,refuelingdatas));
                         smsAdapter adapter= new smsAdapter(getActivity(), R.layout.listnotimsg, smsdatas);

                        SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(adapter);
                        swingBottomInAnimationAdapter.setAbsListView(listmsg);

                        assert swingBottomInAnimationAdapter.getViewAnimator() != null;
                        swingBottomInAnimationAdapter.getViewAnimator().setInitialDelayMillis(
                                Constants.INITIAL_DELAY_MILLIS);

                        listmsg.setAdapter(swingBottomInAnimationAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<smsdataresponse> call, Throwable t) {
                String merror = t.getMessage();
                Toast.makeText(getActivity(), merror, Toast.LENGTH_LONG).show();
                Log.wtf("Transport", "onFailure: "+t.getCause());
            }
        });
    }
}
