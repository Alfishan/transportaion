package com.xeme.transportation.Fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.xeme.transportation.Adapter.RefuellingRecordsAdapter;
import com.xeme.transportation.Bean.RowItem;
import com.xeme.transportation.Interface.SlimIface;
import com.xeme.transportation.POJO.Refuelingdata;
import com.xeme.transportation.POJO.Refuelingdataresponse;
import com.xeme.transportation.R;
import com.xeme.transportation.Refuelling;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.MySharedPreferences;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by root on 23/5/16.
 */
public class Frag_RefuellingRecords extends Fragment {
    FragmentManager fm = getFragmentManager();
    ListView listrefuelling;
    List<RowItem> rowItems;

    MySharedPreferences mypref;
    int tripID = 0,driverid;

//    public static final String[] fueltype = new String[] {"Ethenol","Deasel"};
//    public static final String[] fuelprise = new String[] {"60","50"};
//    public static final String[] odometer = new String[] {"6555","7800"};
//    public static final String[] date = new String[] {"20 may","23 may"};
//    public static final String[] fuellitter = new String[] {"14","10"};
//    public static final String[] totalprice = new String[] {"1000","500"};

    public Frag_RefuellingRecords() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_refuellingrecords, null);

        mypref = MySharedPreferences.getInstance(getActivity(), "Transportation");
        tripID = mypref.getInt(Constants.KEY_TRIP_ID,0);
        driverid = mypref.getInt(Constants.KEY_DRIVER_ID,0);

        listrefuelling = (ListView)root.findViewById(R.id.listrefuelling);
        GetRefuelingdata();
//        rowItems = new ArrayList<RowItem>();
//        for (int i = 0; i < fueltype.length; i++) {
//            RowItem item = new RowItem(fueltype[i], fuelprise[i], odometer[i],date[i],fuellitter[i],totalprice[i]);
//            rowItems.add(item);
//        }

//        RefuellingRecordsAdapter adapter = new RefuellingRecordsAdapter(getActivity(), R.layout.listsinglerefuellingrecords, rowItems);
//        listrefuelling.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) root.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tripID == 0){
                    Toast.makeText(getActivity(), "En primer lugar usted necesita para empezar Nuevo viaje", Toast.LENGTH_SHORT).show();
                }else {
                    Intent i = new Intent(getActivity(), Refuelling.class);
                    startActivity(i);
                }
            }
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        GetRefuelingdata();
    }

    public void GetRefuelingdata(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create())
                // .client(httpClient.build())
                .build();

        SlimIface service = retrofit.create(SlimIface.class);
        System.out.println("0000000000" + driverid);
        Call<Refuelingdataresponse> call = service.getRefuelingdata(driverid);
        System.out.println("1111111111" + call.toString());
        call.enqueue(new Callback<Refuelingdataresponse>() {
            @Override
            public void onResponse(Call<Refuelingdataresponse> call, Response<Refuelingdataresponse> response) {

                if (response != null){
                    Log.wtf("2222222222222", "Response: "+response.message());
                    if (response.body().error.equals(false)) {
                        System.out.println("2222222222" + response.toString());
                        List<Refuelingdata> refuelingdatas = response.body().getResults();
                        System.out.println("3333333333 "+refuelingdatas.toString());

                        // liststarttrip.setAdapter(new StarttripRecordsAdapter(getActivity(),R.layout.liststarttriprecords,refuelingdatas));
                        RefuellingRecordsAdapter adapter = new RefuellingRecordsAdapter(getActivity(), R.layout.listsinglerefuellingrecords, refuelingdatas);

                        SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(adapter);
                        swingBottomInAnimationAdapter.setAbsListView(listrefuelling);

                        assert swingBottomInAnimationAdapter.getViewAnimator() != null;
                        swingBottomInAnimationAdapter.getViewAnimator().setInitialDelayMillis(
                                Constants.INITIAL_DELAY_MILLIS);

                        listrefuelling.setAdapter(swingBottomInAnimationAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<Refuelingdataresponse> call, Throwable t) {
                String merror = t.getMessage();
                Toast.makeText(getActivity(), merror, Toast.LENGTH_LONG).show();
                Log.wtf("Transport", "onFailure: "+t.getCause());
            }
        });
    }
}
