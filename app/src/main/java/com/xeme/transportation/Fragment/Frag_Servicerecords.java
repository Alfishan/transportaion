package com.xeme.transportation.Fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.xeme.transportation.Adapter.ServicedataAdapter;
import com.xeme.transportation.Bean.ExRowItem;
import com.xeme.transportation.Interface.SlimIface;
import com.xeme.transportation.POJO.Servicedata;
import com.xeme.transportation.POJO.Servicedataresponse;
import com.xeme.transportation.R;
import com.xeme.transportation.Service;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.MySharedPreferences;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by root on 23/5/16.
 */
public class Frag_Servicerecords extends Fragment {
//    FragmentManager fm = getFragmentManager();
//    LinearLayout linearLayoutrecords,linearLayoutframe;
    ListView listservice;
    List<ExRowItem> exrowItems;

    MySharedPreferences mypref;
    int tripID = 0,driverid;

//    public static final String[] expensetype = new String[] {"Clutch Fluid","Battery"};
//    public static final String[] odometer = new String[] {"6555","7800"};
//    public static final String[] date = new String[] {"20 may","23 may"};
//    public static final String[] totalprice = new String[] {"1000","5000"};

    public Frag_Servicerecords() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_servicerecords, null);

        mypref = MySharedPreferences.getInstance(getActivity(), "Transportation");
        tripID = mypref.getInt(Constants.KEY_TRIP_ID,0);
        driverid = mypref.getInt(Constants.KEY_DRIVER_ID,0);

        listservice = (ListView)root.findViewById(R.id.listservice);
        GetServicedata();
//        exrowItems = new ArrayList<ExRowItem>();
//        for (int i = 0; i < expensetype.length; i++) {
//            ExRowItem item = new ExRowItem(expensetype[i], odometer[i],date[i], totalprice[i]);
//            exrowItems.add(item);
//        }

//        StarttripRecordsAdapter adapter = new StarttripRecordsAdapter(getActivity(), R.layout.liststarttriprecords, exrowItems);
//        listservice.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) root.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tripID == 0){
                    Toast.makeText(getActivity(), "En primer lugar usted necesita para empezar Nuevo viaje", Toast.LENGTH_SHORT).show();
                }else {
                    Intent i = new Intent(getActivity(), Service.class);
                    startActivity(i);
                }
//                linearLayoutrecords.setVisibility(View.INVISIBLE);
//                linearLayoutframe.setVisibility(View.VISIBLE);

            }
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        GetServicedata();
    }

    public void GetServicedata(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create())
                // .client(httpClient.build())
                .build();

        SlimIface service = retrofit.create(SlimIface.class);
        System.out.println("0000000000" + driverid);
        Call<Servicedataresponse> call = service.getServicedata(driverid);
        System.out.println("1111111111" + call.toString());
        call.enqueue(new Callback<Servicedataresponse>() {
            @Override
            public void onResponse(Call<Servicedataresponse> call, Response<Servicedataresponse> response) {

                if (response != null){
                   // Log.wtf("2222222222222", "Response: "+response.message());
                    if (response.body().error.equals(false)) {
                       // System.out.println("2222222222" + response.toString());
                        List<Servicedata> servicedatas = response.body().getResults();
                      //  System.out.println("3333333333 "+servicedatas.toString());

                        // liststarttrip.setAdapter(new StarttripRecordsAdapter(getActivity(),R.layout.liststarttriprecords,refuelingdatas));
                        ServicedataAdapter adapter = new ServicedataAdapter(getActivity(), R.layout.listservicedata, servicedatas);

                        SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(adapter);
                        swingBottomInAnimationAdapter.setAbsListView(listservice);

                        assert swingBottomInAnimationAdapter.getViewAnimator() != null;
                        swingBottomInAnimationAdapter.getViewAnimator().setInitialDelayMillis(
                                Constants.INITIAL_DELAY_MILLIS);

                        listservice.setAdapter(swingBottomInAnimationAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<Servicedataresponse> call, Throwable t) {
                String merror = t.getMessage();
                Toast.makeText(getActivity(), merror, Toast.LENGTH_LONG).show();
                Log.wtf("Transport", "onFailure: "+t.getCause());
            }
        });
    }
}
