package com.xeme.transportation.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.xeme.transportation.Adapter.AssigntripAdapter;
import com.xeme.transportation.Interface.SlimIface;
import com.xeme.transportation.POJO.Assigntripdata;
import com.xeme.transportation.POJO.smsAssignTripresponse;
import com.xeme.transportation.R;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.MySharedPreferences;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by root on 4/8/16.
 */
public class Frag_NotiAssignTrip extends Fragment {

    ListView listtripmsg;
    MySharedPreferences mypref;
    int driverid;

    public Frag_NotiAssignTrip() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_notiassigntrip, null);
        mypref = MySharedPreferences.getInstance(getActivity(), "Transportation");
        driverid = mypref.getInt(Constants.KEY_DRIVER_ID,0);

        listtripmsg = (ListView)root.findViewById(R.id.listtripmsg);

        GetTripData();

        return root;
    }
    public void GetTripData(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create())
                // .client(httpClient.build())
                .build();

        SlimIface service = retrofit.create(SlimIface.class);
        System.out.println("0000000000" + driverid);
        Call<smsAssignTripresponse> call = service.getassigntripdata(driverid);
        System.out.println("1111111111" + call.toString());
        call.enqueue(new Callback<smsAssignTripresponse>() {
            @Override
            public void onResponse(Call<smsAssignTripresponse> call, Response<smsAssignTripresponse> response) {

                if (response != null){
                    Log.wtf("2222222222222", "Response: "+response.message());
                    if (response.body().error.equals(false)) {
                        System.out.println("2222222222" + response.toString());
                        List<Assigntripdata> tripdatas = response.body().getResults();
                        System.out.println("3333333333 "+tripdatas.toString());

                        // liststarttrip.setAdapter(new StarttripRecordsAdapter(getActivity(),R.layout.liststarttriprecords,refuelingdatas));
                        AssigntripAdapter adapter= new AssigntripAdapter(getActivity(), R.layout.listnotiassigntrip, tripdatas);

                        SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(adapter);
                        swingBottomInAnimationAdapter.setAbsListView(listtripmsg);

                        assert swingBottomInAnimationAdapter.getViewAnimator() != null;
                        swingBottomInAnimationAdapter.getViewAnimator().setInitialDelayMillis(
                                Constants.INITIAL_DELAY_MILLIS);

                        listtripmsg.setAdapter(swingBottomInAnimationAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<smsAssignTripresponse> call, Throwable t) {
                String merror = t.getMessage();
                Toast.makeText(getActivity(), merror, Toast.LENGTH_LONG).show();
                Log.wtf("Transport", "onFailure: "+t.getCause());
            }
        });
    }
}

