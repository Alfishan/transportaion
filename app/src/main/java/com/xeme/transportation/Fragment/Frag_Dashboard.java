package com.xeme.transportation.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.xeme.transportation.Adapter.TripLogsAdapter;
import com.xeme.transportation.Interface.SlimIface;
import com.xeme.transportation.POJO.TripLogsdata;
import com.xeme.transportation.POJO.TripLogsresponse;
import com.xeme.transportation.R;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.MySharedPreferences;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by root on 23/5/16.
 */
public class Frag_Dashboard extends Fragment {

    public ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    MySharedPreferences mypref;

    private ListView mRecyclerView;

  //  private TimeLineAdapter mTimeLineAdapter;
    int driverid;

 //   private List<TimeLineModel> mDataList = new ArrayList<>();

    public Frag_Dashboard() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_dashboard, null);
        mypref = MySharedPreferences.getInstance(getActivity(),"Transportation");
        driverid = mypref.getInt(Constants.KEY_DRIVER_ID,0);
        String name = mypref.getString(Constants.KEY_DRIVER_NAME,"FSG");
        String VEHICLE_BRAND = mypref.getString(Constants.KEY_VEHICLE_BRAND,"");
        getActionBar().setTitle(name);
        getActionBar().setSubtitle(VEHICLE_BRAND);

        mRecyclerView = (ListView) root.findViewById(R.id.recyclerView);

        GetTripLogsData();
//        initView();

//        FloatingActionButton fab = (FloatingActionButton) root.findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        return root;
    }

//    private void initView() {
//
//        for(int i = 0;i < 2;i++) {
//            TimeLineModel model = new TimeLineModel();
//            model.setName("Date"+i);
//            //model.setAge(i);
//            mDataList.add(model);
//        }
//
//        mTimeLineAdapter = new TimeLineAdapter(mDataList);
//        //mRecyclerView.setAdapter((ListAdapter) mTimeLineAdapter);
//    }

    public void GetTripLogsData(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create())
                // .client(httpClient.build())
                .build();

        SlimIface service = retrofit.create(SlimIface.class);
        System.out.println("0000000000" + driverid);
        Call<TripLogsresponse> call = service.getTripLogsdata(driverid);
        System.out.println("1111111111" + call.toString());
        call.enqueue(new Callback<TripLogsresponse>() {
            @Override
            public void onResponse(Call<TripLogsresponse> call, Response<TripLogsresponse> response) {

                if (response != null){
                    Log.wtf("2222222222222", "Response: "+response.message());
                    if (response.body().error.equals(false)) {
                        System.out.println("2222222222" + response.toString());
                        List<TripLogsdata> triplogsdata = response.body().getResults();
                        System.out.println("3333333333 "+triplogsdata.toString());

                        // liststarttrip.setAdapter(new StarttripRecordsAdapter(getActivity(),R.layout.liststarttriprecords,refuelingdatas));
                        TripLogsAdapter adapter = new TripLogsAdapter(getActivity(), R.layout.item_timeline, triplogsdata);

                        SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(adapter);
                        swingBottomInAnimationAdapter.setAbsListView(mRecyclerView);

                        assert swingBottomInAnimationAdapter.getViewAnimator() != null;
                        swingBottomInAnimationAdapter.getViewAnimator().setInitialDelayMillis(
                                Constants.INITIAL_DELAY_MILLIS);

                        mRecyclerView.setAdapter(swingBottomInAnimationAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<TripLogsresponse> call, Throwable t) {
                String merror = t.getMessage();
                Toast.makeText(getActivity(), merror, Toast.LENGTH_LONG).show();
                Log.wtf("Transport", "onFailure: "+t.getCause());
            }
        });
    }
}
