package com.xeme.transportation.Fragment;

/**
 * Created by root on 23/5/16.
 */

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xeme.transportation.R;

public class Frag_Refuelling extends Fragment {

    public Frag_Refuelling() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_refuelling, null);
        return root;
    }

}
