package com.xeme.transportation.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xeme.transportation.R;

/**
 * Created by root on 23/5/16.
 */
public class Frag_Expense extends Fragment {

    public Frag_Expense() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_expense, null);
        return root;
    }

}
