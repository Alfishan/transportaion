package com.xeme.transportation;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.xeme.transportation.DateTimePicker.SlideDateTimeListener;
import com.xeme.transportation.DateTimePicker.SlideDateTimePicker;
import com.xeme.transportation.Interface.SlimIface;
import com.xeme.transportation.POJO.StartDriverTripModel;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.Gpslocation;
import com.xeme.transportation.utils.MySharedPreferences;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Service extends AppCompatActivity {

    String[] location = {"Local"};
    Spinner spinnerservicelocation;
    Button btn_submit;

    EditText edtservicedate, edtserviceOdometer, edtservicetype, edtservicevalue, edtservicenotes;
    private SimpleDateFormat mFormatter = new SimpleDateFormat("dd MM yyyy hh:mm aa");
    MySharedPreferences mypref;
    int cID,dID,tripID,vid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);

        mypref = MySharedPreferences.getInstance(this, "Transportation");
        cID = mypref.getInt(Constants.KEY_CUSTOMER_ID,0);
        dID = mypref.getInt(Constants.KEY_DRIVER_ID,0);
        tripID = mypref.getInt(Constants.KEY_TRIP_ID,0);
        vid = mypref.getInt(Constants.KEY_VEHICLE_ID,0);

        edtservicedate = (EditText) findViewById(R.id.ServiceDate);
        edtserviceOdometer = (EditText)findViewById(R.id.ServiceOdometer);
        edtservicetype = (EditText)findViewById(R.id.Servicetypeofservice);
        edtservicevalue = (EditText)findViewById(R.id.Servicevalue);
        edtservicenotes = (EditText)findViewById(R.id.ServiceExtranote);

        spinnerservicelocation = (Spinner)findViewById(R.id.SpinnerServicelocation);
        btn_submit = (Button)findViewById(R.id.btn_Servicesubmit);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, location);
        spinnerservicelocation.setAdapter(adapter);

        edtservicedate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .build()
                        .show();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Gpslocation myloc = new Gpslocation(Service.this);
                LatLng l = myloc.getlatlong();
                double lat = l.latitude;
                double lng = l.longitude;

                if (CheckFieldValidation()) {
                    String servicedate = edtservicedate.getText().toString();
                    String serviceodometer = edtserviceOdometer.getText().toString();
                    String servicetype = edtservicetype.getText().toString();
                    String servicevalue = edtservicevalue.getText().toString();
                    // String servicelocation = spinnerservicelocation.getSelectedItem().toString();
                    String servicenotes = edtservicenotes.getText().toString();

                    InsertServices(cID, dID, tripID, servicedate, serviceodometer, servicetype, servicevalue, servicenotes,lat,lng,vid);
                }
            }
        });
    }
    private boolean CheckFieldValidation(){
        boolean valid=true;
        if(edtservicedate.getText().toString().equals("")){
            edtservicedate.setError("no puede estar vacía");
            valid=false;
        }else if(edtserviceOdometer.getText().toString().equals("")){
            edtserviceOdometer.setError("no puede estar vacía");
            valid=false;
        }else if (edtservicetype.getText().toString().equals("")){
            edtservicetype.setError("no puede estar vacía");
            valid=false;
        }else if (edtservicevalue.getText().toString().equals("")){
            edtservicevalue.setError("no puede estar vacía");
            valid=false;
        }else if (edtservicenotes.getText().toString().equals("")){
            edtservicenotes.setError("no puede estar vacía");
            valid=false;
        }
        return valid;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }
    private SlideDateTimeListener listener = new SlideDateTimeListener() {

        @Override
        public void onDateTimeSet(Date date)
        {
            edtservicedate.setText(mFormatter.format(date));
           // Toast.makeText(Service.this,mFormatter.format(date), Toast.LENGTH_SHORT).show();
        }
        // Optional cancel listener
        @Override
        public void onDateTimeCancel()
        {
            Toast.makeText(Service.this,
                    "Cancelado", Toast.LENGTH_SHORT).show();
        }
    };
    public void InsertServices(int cid,int did,int tripid,String sservicedate,String sserviceodometer,String sservicetype,String sservicevalue,String sservicenotes,double lat,double lang,int vid){

        final ProgressDialog loading = ProgressDialog.show(this,"Cargando","por favor espera...",false,false);
//        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
//        final SharedPreferences.Editor editor = pref.edit();

        // OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create())
                // .client(httpClient.build())
                .build();

        SlimIface service = retrofit.create(SlimIface.class);
        Call<StartDriverTripModel> call = service.Serviceinsert(cid,did,tripid,sservicedate,sserviceodometer,sservicetype,sservicevalue,sservicenotes,lat,lang,vid);
        call.enqueue(new Callback<StartDriverTripModel>() {
            @Override
            public void onResponse(Call<StartDriverTripModel> call, Response<StartDriverTripModel> response) {

                Log.d("sssssssssssssss",response.toString());
                System.out.println("sssssssssssssssss" + response.toString());

                if (response != null) {
                    Log.wtf("Transport insert Service", "Response: "+response.message());
                    if (response.body().error.equals(false)) {  //insert Success
                        loading.dismiss();
                        Toast.makeText(Service.this, "Servicio insertado con éxito", Toast.LENGTH_SHORT).show();
//                        mypref.putInt(Constants.KEY_TRIP_ID, response.body().tripid);
//                        mypref.commit();
                        onBackPressed();

                    } else // insert failure
                    {
                        loading.dismiss();
                        Toast.makeText(Service.this, "Por favor, insertar datos correcta ", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    loading.dismiss();
                    Toast.makeText(Service.this,response.body().toString() , Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<StartDriverTripModel> call, Throwable t) {
                String merror = t.getMessage();
                Toast.makeText(Service.this, merror, Toast.LENGTH_LONG).show();
                Log.wtf("Service failuer","onFailure"+merror+"  : "+t.getCause());
            }
        });
    }
}
