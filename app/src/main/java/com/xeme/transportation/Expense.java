package com.xeme.transportation;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.xeme.transportation.DateTimePicker.SlideDateTimeListener;
import com.xeme.transportation.DateTimePicker.SlideDateTimePicker;
import com.xeme.transportation.Interface.SlimIface;
import com.xeme.transportation.POJO.StartDriverTripModel;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.Gpslocation;
import com.xeme.transportation.utils.MySharedPreferences;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Expense extends AppCompatActivity {

    String[] location = {"Local"};
    String[] reason = {"Reason","Trip","Work"};
    Spinner spinnerexpenselocation,spinnerexpensereason;
    EditText edtexpensedate, edtexpenseOdometer, edtexpensetype, edtexpensevalue, edtexpensenotes;
    Button btnsubmitexpense;
    private SimpleDateFormat mFormatter = new SimpleDateFormat("dd MM yyyy hh:mm aa");
    MySharedPreferences mypref;
    int cID,dID,tripID,vid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);

        mypref = MySharedPreferences.getInstance(this, "Transportation");
        cID = mypref.getInt(Constants.KEY_CUSTOMER_ID,0);
        dID = mypref.getInt(Constants.KEY_DRIVER_ID,0);
        tripID = mypref.getInt(Constants.KEY_TRIP_ID,0);
        vid = mypref.getInt(Constants.KEY_VEHICLE_ID,0);

        edtexpensedate = (EditText) findViewById(R.id.ExpenseDate);
        edtexpenseOdometer = (EditText)findViewById(R.id.ExpenseOdometer);
        edtexpensetype = (EditText)findViewById(R.id.Expensetypeofexpense);
        edtexpensevalue = (EditText)findViewById(R.id.Expensevalue);
        edtexpensenotes = (EditText)findViewById(R.id.ExpenseExtranote);
        btnsubmitexpense = (Button)findViewById(R.id.btn_Expensesubmit);

        spinnerexpenselocation = (Spinner)findViewById(R.id.Expenselocation);
        spinnerexpensereason = (Spinner)findViewById(R.id.Expensereson);
        ArrayAdapter<String> adapterlocation = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, location);
        spinnerexpenselocation.setAdapter(adapterlocation);

        ArrayAdapter<String> adapterreason = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, reason);
        spinnerexpensereason.setAdapter(adapterreason);

        edtexpensedate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        .build()
                        .show();
            }
        });

        btnsubmitexpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Gpslocation myloc = new Gpslocation(Expense.this);
                LatLng l = myloc.getlatlong();
                double lat = l.latitude;
                double lng = l.longitude;

                if (CheckFieldValidation()) {
                    String expensedate = edtexpensedate.getText().toString();
                    String expenseodometer = edtexpenseOdometer.getText().toString();
                    String expensetype = edtexpensetype.getText().toString();
                    String expensevalue = edtexpensevalue.getText().toString();
                    String expenselocation = spinnerexpenselocation.getSelectedItem().toString();
                    String expensereason = spinnerexpensereason.getSelectedItem().toString();
                    String expensenotes = edtexpensenotes.getText().toString();

                    InsertExpesne(cID, dID, tripID, expensedate, expenseodometer, expensetype, expensevalue, expenselocation, expensereason, expensenotes,lat, lng, vid);
                }
            }
        });

    }
    private boolean CheckFieldValidation(){
        boolean valid=true;
        if(edtexpensedate.getText().toString().equals("")){
            edtexpensedate.setError("no puede estar vacía");
            valid=false;
        }else if(edtexpenseOdometer.getText().toString().equals("")){
            edtexpenseOdometer.setError("no puede estar vacía");
            valid=false;
        }else if (edtexpensetype.getText().toString().equals("")){
            edtexpensetype.setError("no puede estar vacía");
            valid=false;
        }else if (edtexpensevalue.getText().toString().equals("")){
            edtexpensevalue.setError("no puede estar vacía");
            valid=false;
        }else if (edtexpensenotes.getText().toString().equals("")){
            edtexpensenotes.setError("no puede estar vacía");
            valid=false;
        }
        return valid;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }
    private SlideDateTimeListener listener = new SlideDateTimeListener() {

        @Override
        public void onDateTimeSet(Date date)
        {
            edtexpensedate.setText(mFormatter.format(date));
           // Toast.makeText(Expense.this,mFormatter.format(date), Toast.LENGTH_SHORT).show();
        }
        // Optional cancel listener
        @Override
        public void onDateTimeCancel()
        {
            Toast.makeText(Expense.this,
                    "Cancelado", Toast.LENGTH_SHORT).show();
        }
    };
    public void InsertExpesne(int cid,int did,int tripid,String sexpensedate,String sexpenseodometer,String sexpensetype,String sexpensevalue,String sservicelocation,String sexpensereason,String sexpensenotes,double lat,double lang,int vid){

        final ProgressDialog loading = ProgressDialog.show(this,"Cargando","por favor espera...",false,false);
//        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
//        final SharedPreferences.Editor editor = pref.edit();

        // OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create())
                // .client(httpClient.build())
                .build();

        SlimIface service = retrofit.create(SlimIface.class);
        Call<StartDriverTripModel> call = service.Expenseinsert(cid,did,tripid,sexpensedate,sexpenseodometer,sexpensetype,sexpensevalue,sservicelocation,sexpensereason,sexpensenotes,lat,lang,vid);
        call.enqueue(new Callback<StartDriverTripModel>() {
            @Override
            public void onResponse(Call<StartDriverTripModel> call, Response<StartDriverTripModel> response) {

                if (response != null) {
                    Log.wtf("Transport insert Expense", "Response: "+response.message());
                    if (response.body().error.equals(false)) {  //insert Success
                        loading.dismiss();
                        Toast.makeText(Expense.this, "Gasto insertado con éxito", Toast.LENGTH_SHORT).show();
//                        mypref.putInt(Constants.KEY_TRIP_ID, response.body().tripid);
//                        mypref.commit();
                        onBackPressed();

                    } else // insert failure
                    {
                        loading.dismiss();
                        Toast.makeText(Expense.this, "Por favor, insertar datos correcta ", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    loading.dismiss();
                    Toast.makeText(Expense.this,response.body().toString() , Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<StartDriverTripModel> call, Throwable t) {
                loading.dismiss();
                String merror = t.getMessage();
                Toast.makeText(Expense.this, merror, Toast.LENGTH_LONG).show();
                Log.wtf("Service failuer", "onFailure: "+t.getCause());
            }
        });
    }
}
