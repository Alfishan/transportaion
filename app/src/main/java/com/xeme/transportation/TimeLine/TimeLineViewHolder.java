package com.xeme.transportation.TimeLine;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.xeme.transportation.R;

/**
 * Created by root on 24/5/16.
 */
public class TimeLineViewHolder extends RecyclerView.ViewHolder {
    public TextView name,location,odometer,date,time;
    public  TimelineView mTimelineView;

    public TimeLineViewHolder(View itemView, int viewType) {
        super(itemView);
        //name = (TextView) itemView.findViewById(R.id.txtoperation);
        //location = (TextView) itemView.findViewById(R.id.txtextranotes);
        odometer = (TextView) itemView.findViewById(R.id.txtinitialodometer);
        date = (TextView) itemView.findViewById(R.id.txtlogdate);
        //time = (TextView) itemView.findViewById(R.id.tx_time);

        mTimelineView = (TimelineView) itemView.findViewById(R.id.time_marker);
        mTimelineView.initLine(viewType);
    }

}
