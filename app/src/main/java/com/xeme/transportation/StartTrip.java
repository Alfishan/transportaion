package com.xeme.transportation;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.xeme.transportation.DateTimePicker.SlideDateTimeListener;
import com.xeme.transportation.DateTimePicker.SlideDateTimePicker;
import com.xeme.transportation.Interface.SlimIface;
import com.xeme.transportation.POJO.StartDriverTripModel;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.Gpslocation;
import com.xeme.transportation.utils.MySharedPreferences;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StartTrip extends AppCompatActivity {
    public static final int PLACE_PICKER_REQUEST = 92;
    public static final int PLACE_PICKER_REQUEST2 = 93;
    public static final int CAMERA_REQ_CODE = 94;
    String[] reason = {"Tipo", "Entrega", "Recolección", "Personas", "VIP"};
    Spinner spinnerreson;
    Button btnSubmit;
    MySharedPreferences mypref;
    EditText edtStartLocation, edtInitialOdometer, edtDate, edtDestinationLocation, edtNotes;
    @BindView(R.id.StarttripLocation)
    EditText StarttripLocation;
    @BindView(R.id.StarttripOdometer)
    EditText StarttripOdometer;
    @BindView(R.id.StarttripDestinationLocation)
    EditText StarttripDestinationLocation;
    int cID, dID, vid;

    @BindView(R.id.StarttripPackageImg)
    AppCompatImageView StarttripPackegeImg;
    @BindView(R.id.StarttripPackagebtn)
    AppCompatButton StarttripPackagebtn;

    Bitmap bitmap;

    private SimpleDateFormat mFormatter = new SimpleDateFormat("dd MM yyyy hh:mm aa");
    private SlideDateTimeListener listener = new SlideDateTimeListener() {

        @Override
        public void onDateTimeSet(Date date) {
            edtDate.setText(mFormatter.format(date));
            Toast.makeText(StartTrip.this,
                    mFormatter.format(date), Toast.LENGTH_SHORT).show();
        }

        // Optional cancel listener
        @Override
        public void onDateTimeCancel() {
            Toast.makeText(StartTrip.this,
                    "Cancelado", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_trip);
        ButterKnife.bind(this);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);


        mypref = MySharedPreferences.getInstance(this, "Transportation");
        cID = mypref.getInt(Constants.KEY_CUSTOMER_ID, 0);
        dID = mypref.getInt(Constants.KEY_DRIVER_ID, 0);
        vid = mypref.getInt(Constants.KEY_VEHICLE_ID, 0);

        edtStartLocation = (EditText) findViewById(R.id.StarttripLocation);
        edtInitialOdometer = (EditText) findViewById(R.id.StarttripOdometer);
        edtDate = (EditText) findViewById(R.id.StarttripDate);
        // edtTime = (EditText) findViewById(R.id.StarttripTime);
        edtDestinationLocation = (EditText) findViewById(R.id.StarttripDestinationLocation);
        edtNotes = (EditText) findViewById(R.id.StarttripExtranote);

        btnSubmit = (Button) findViewById(R.id.btn_Starttripsubmit);

        Gpslocation myloc = new Gpslocation(StartTrip.this);
        LatLng l = myloc.getlatlong();
        double lat = l.latitude;
        double lng = l.longitude;
        mypref.putDouble(Constants.MAPSTARTLAT, lat);
        mypref.putDouble(Constants.MAPSTARTLONG, lng);
        mypref.commit();
        //  String add = myloc.getAddressFromLocation(l,Drawer.this,);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            edtStartLocation.setText(extras.getString("starttriploc"));
            edtDestinationLocation.setText(extras.getString("endtriploc"));
        } else {
            SetCurrentLocationonStarttrip(l);
        }

        edtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        //.setMinDate(minDate)
                        //.setMaxDate(maxDate)
                        //.setIs24HourTime(true)
                        //.setTheme(SlideDateTimePicker.HOLO_DARK)
                        //.setIndicatorColor(Color.parseColor("#990000"))
                        .build()
                        .show();
            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(StartTrip.this, "" + cID +" : " + dID, Toast.LENGTH_LONG).show();
                String encodeImage = null;
                if (bitmap != null){
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                    encodeImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                }

                if (CheckFieldValidation()) {
                    String startlocation = edtStartLocation.getText().toString();
                    String initialodometer = edtInitialOdometer.getText().toString();
                    String datetimeonstart = edtDate.getText().toString();
                    String destinationlocation = edtDestinationLocation.getText().toString();
                    String reason = spinnerreson.getSelectedItem().toString();
                    String extranotesonstart = edtNotes.getText().toString();
                    int isstart = 1;

                    StartDriverTrip(dID, cID, startlocation, initialodometer, datetimeonstart, destinationlocation, reason, extranotesonstart, encodeImage, isstart, vid);
                }
//                TripModel tripModel = new TripModel();
//                tripModel.setStartlocation(edtStartLocation.getText().toString());
//                tripModel.setInitialodometer(edtInitialOdometer.getText().toString());
//                tripModel.setDate(edtDate.getText().toString());
//                tripModel.setTime(edtTime.getText().toString());
//                tripModel.setDestinationlocation(edtDestinationLocation.getText().toString());
//                tripModel.setReason(spinnerreson.getSelectedItem().toString());
//                tripModel.setNotes(edtNotes.getText().toString());
//                tripModel.setIsstart(true);
//                tripModel.save();

            }
        });

        spinnerreson = (Spinner) findViewById(R.id.SpinnerStarttripreson);
        ArrayAdapter<String> adapterreason = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, reason);
        spinnerreson.setAdapter(adapterreason);
    }

    public void SetCurrentLocationonStarttrip(LatLng latLng) {

        double lat = latLng.latitude;
        double lng = latLng.longitude;

        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);

            if (addresses != null) {
                Address returnedAddress = addresses.get(0);

                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                edtStartLocation.setText(strReturnedAddress.toString());
                // Toast.makeText(StartTrip.this, "Address : " + strReturnedAddress.toString(), Toast.LENGTH_SHORT).show();
                System.out.println("Adderes " + strReturnedAddress.toString());
            } else {
                //Toast.makeText(StartTrip.this, "No Address returned!", Toast.LENGTH_SHORT).show();
                edtStartLocation.setText("No Address returned!");
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //Toast.makeText(StartTrip.this, "Canont get Address! " , Toast.LENGTH_SHORT).show();
            edtStartLocation.setText("Canont get Address!");
        }
    }

    private boolean CheckFieldValidation() {
        boolean valid = true;
        if (edtStartLocation.getText().toString().equals("")) {
            edtStartLocation.setError("no puede estar vacía");
            valid = false;
        } else if (edtInitialOdometer.getText().toString().equals("")) {
            edtInitialOdometer.setError("no puede estar vacía");
            valid = false;
        } else if (edtDate.getText().toString().equals("")) {
            edtDate.setError("no puede estar vacía");
            valid = false;
        } else if (edtDestinationLocation.getText().toString().equals("")) {
            edtDestinationLocation.setError("no puede estar vacía");
            valid = false;
        } else if (edtNotes.getText().toString().equals("")) {
            edtNotes.setError("no puede estar vacía");
            valid = false;
        } else if (bitmap == null){
            StarttripPackagebtn.setError("Por favor captura de imágenes");
            valid = false;
        }
        return valid;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void StartDriverTrip(int did, int cid, final String startlocation, String initialodometer, String datetimeonstart, final String destinationlocation, String reason, String extranotesonstart, String packageimg, int isstart, int vid) {

        final ProgressDialog loading = ProgressDialog.show(this, "Cargando", "por favor espera...", false, false);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        final SharedPreferences.Editor editor = pref.edit();

        // OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create())
                // .client(httpClient.build())
                .build();

        SlimIface service = retrofit.create(SlimIface.class);
        Call<StartDriverTripModel> call = service.StartDriverTrip(did, cid, startlocation, initialodometer, datetimeonstart, destinationlocation, reason, extranotesonstart, packageimg, isstart, vid);
        call.enqueue(new Callback<StartDriverTripModel>() {
            @Override
            public void onResponse(Call<StartDriverTripModel> call, Response<StartDriverTripModel> response) {

                if (response != null) {
                    Log.wtf("Transport insert trip", "Response: " + response.message());
                    if (response.body().error.equals(false)) {  //insert Success

                        Toast.makeText(StartTrip.this, "Viaje iniciado con éxito", Toast.LENGTH_SHORT).show();
                        mypref.putInt(Constants.KEY_TRIP_ID, response.body().tripid);
                        //mypref.commit();

                        editor.putInt("Toggle", 1);
                        mypref.commit();
                        mypref.putBoolean(Constants.IsTripStarted, true);
                        mypref.commit();
                        editor.commit();
//                        mypref.putString(Constants.MAPSTARTLAT,startlocation);
//                        mypref.putString(Constants.MAPStopLANG,destinationlocation);
//                        mypref.commit();
                        // getLocationFromAddress(startlocation,0);
                        // getLocationFromAddress(destinationlocation,1);

                        Intent i = new Intent(StartTrip.this, Drawer.class);
                        startActivity(i);
                        StartTrip.this.finish();
                        loading.dismiss();

                    } else // insert failure
                    {
                        loading.dismiss();
                        Toast.makeText(StartTrip.this, "Por favor, insertar datos correcta ", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    loading.dismiss();
                    Toast.makeText(StartTrip.this, response.body().toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StartDriverTripModel> call, Throwable t) {
                loading.dismiss();
                String merror = t.getMessage();
                Toast.makeText(StartTrip.this, merror, Toast.LENGTH_LONG).show();
                Log.wtf("Transport", "onFailure: " + t.getCause());
            }
        });
    }

    public void getLocationFromAddress(String strAddress, int i) {

        Geocoder coder = new Geocoder(this);
        List<Address> address;
        //Barcode.GeoPoint p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {

            }
            Address location = address.get(0);
            if (i == 0) {
                mypref.putString(Constants.MAPSTARTLAT, String.valueOf(location.getLatitude()));
                //mypref.putString(Constants.MAPSTARTLANG, String.valueOf(location.getLongitude()));
            } else {
                // mypref.putString(Constants.MAPStopLAT, String.valueOf(location.getLatitude()));
                // mypref.putString(Constants.MAPStopLANG, String.valueOf(location.getLongitude()));
            }
//            p1 = new Barcode.GeoPoint((int) (location.getLatitude() * 1E6),
//                    (int) (location.getLongitude() * 1E6));
            // return p1;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.StarttripLocation, R.id.StarttripOdometer, R.id.StarttripDestinationLocation, R.id.StarttripPackageImg, R.id.StarttripPackagebtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.StarttripLocation:
                PickAPlace(PLACE_PICKER_REQUEST);
                break;
            case R.id.StarttripDestinationLocation:
                PickAPlace(PLACE_PICKER_REQUEST2);
                break;
            case R.id.StarttripOdometer:
                break;
            case R.id.StarttripPackageImg:
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAMERA_REQ_CODE);
                break;
            case R.id.StarttripPackagebtn:
                Intent intentbtn = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intentbtn, CAMERA_REQ_CODE);
                break;
        }
    }



 /*   void StartTrip(){


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder();
        // add your other interceptors …

        // add logging as last interceptor
        httpClient.addInterceptor(logging);  // <-- this is the important line!

        String url = "http://fsg.grip.com.mx";

        HttpUrl u= HttpUrl.parse(url);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        SlimIface service = retrofit.create(SlimIface.class);
        Call<LoginModel> call = service.GetLoginDetails(username, password, userid);
        call.enqueue(new Callback<LoginModel>() {o
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {

                if (response != null) {
                    Log.wtf("Transport", "Response: "+response.message());
                    if (response.body().error.equals(false)) {  //login Success

                        Toast.makeText(StartTrip.this, "Login In SuccessFully", Toast.LENGTH_SHORT).show();

                        mypref.putBoolean(Constants.IsTripStarted,true);


                        mypref.putInt("MyDID", response.body().did);
                        mypref.putInt("MyCID", response.body().cid);
                        mypref.putString("api_key", response.body().apikey);
                        mypref.putString("name", response.body().name);

                        System.out.println("PREFlogin" + response.body().did);
                        mypref.commit();

                        Intent Serviceintent = new Intent(StartTrip.this, MyLocationService.class);
                        startService(Serviceintent);
                        Intent i = new Intent(StartTrip.this, Drawer.class);
                        startActivity(i);


                    } else // login failure
                    {
                        Toast.makeText(StartTrip.this, "Invalid UserName/Pass or DiverID ", Toast.LENGTH_SHORT).show();
                    }
                }

                else {

                    Toast.makeText(StartTrip.this,response.body().toString() , Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {

                String merror = t.getMessage();
                Toast.makeText(StartTrip.this, merror, Toast.LENGTH_LONG).show();
                Log.wtf("Transport", "onFailure: "+t.getCause());
            }
        });


    }*/


    void PickAPlace(int reqcode) {

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(this), reqcode);
        } catch (GooglePlayServicesRepairableException e) {
            Log.wtf(Constants.TAG, "********Error******* :" + e);
        } catch (GooglePlayServicesNotAvailableException s) {
            Log.wtf(Constants.TAG, "********Error******* :" + s);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place selectedPlace = PlacePicker.getPlace(this, data);
                mypref.putDouble(Constants.MAPSTARTLAT, selectedPlace.getLatLng().latitude);
                mypref.putDouble(Constants.MAPSTARTLONG, selectedPlace.getLatLng().longitude);
                mypref.commit();
                //  Toast.makeText(StartTrip.this, "Selected Place" + selectedPlace.getAddress(), Toast.LENGTH_SHORT).show();
                SetAddress(selectedPlace.getName() + "," + selectedPlace.getAddress().toString(), 0);
                // Log.wtf(Constants.TAG, "********Selected Origin Address******* :" + selectedPlace.getName()+","+ selectedPlace.getAddress().toString());
                // Do something with the place
            }
        }
        if (requestCode == PLACE_PICKER_REQUEST2) {
            if (resultCode == RESULT_OK) {
                Place selectedPlace = PlacePicker.getPlace(this, data);
                mypref.putDouble(Constants.MAPStopLAt, selectedPlace.getLatLng().latitude);
                mypref.putDouble(Constants.MAPStopLong, selectedPlace.getLatLng().longitude);
                mypref.commit();
                //  Toast.makeText(StartTrip.this, "Selected Place" + selectedPlace.getAddress(), Toast.LENGTH_SHORT).show();
                SetAddress(selectedPlace.getName() + "," + selectedPlace.getAddress().toString(), 1);
                // Log.wtf(Constants.TAG, "********Selected Destination Address******* :" + selectedPlace.getName()+","+ selectedPlace.getAddress().toString());
                // Do something with the place
            }
        }
        if (requestCode == CAMERA_REQ_CODE){
            Bundle extra = data.getExtras();
            if (extra != null) {
                StarttripPackagebtn.setVisibility(View.GONE);
                StarttripPackegeImg.setVisibility(View.VISIBLE);
                bitmap = (Bitmap) data.getExtras().get("data");
                StarttripPackegeImg.setImageBitmap(bitmap);
            }
        }
    }

  /*  @Override
    protected void onStart() {
        super.onStart();
        mClient.connect();
    }
    @Override
    protected void onStop() {
        mClient.disconnect();
        super.onStop();
    }*/


    void SetAddress(String address, int forwho) {
        switch (forwho) {

            case 0:
                edtStartLocation.setText("");
                edtStartLocation.setText(address);
                break;
            case 1:
                edtDestinationLocation.setText("");
                edtDestinationLocation.setText(address);
                break;
        }
    }

}
