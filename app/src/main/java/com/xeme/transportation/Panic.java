package com.xeme.transportation;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.xeme.transportation.Interface.SlimIface;
import com.xeme.transportation.MultiSelectSpineer.MultiSelectionSpinner;
import com.xeme.transportation.POJO.LogoutModel;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.MySharedPreferences;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Panic extends AppCompatActivity {

    Bitmap bitmap;
    public static final int CAMERA_REQ_CODE = 94;
    MySharedPreferences mypref;
    int d_id, v_id;

    @BindView(R.id.btncaptimgpanic)
    AppCompatButton btncaptimgpanic;
    @BindView(R.id.btnsendpanic)
    AppCompatButton btnsendpanic;
    @BindView(R.id.imgpanic)
    AppCompatImageView imgpanic;
    @BindView(R.id.input1)
    MultiSelectionSpinner input1;
    @BindView(R.id.edtpanicdetail)
    EditText edtpanicdetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panic);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);
        mypref = MySharedPreferences.getInstance(this, "Transportation");
        ButterKnife.bind(this);

        v_id = mypref.getInt(Constants.KEY_VEHICLE_ID, 0);
        d_id = mypref.getInt(Constants.KEY_DRIVER_ID, 0);


        List<String> list = new ArrayList<String>();
        list.add("Robbery/Assault");
        list.add("Police");
        list.add("Accident");
        input1.setItems(list);

    }
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAMERA_REQ_CODE) {
            Bundle extra = data.getExtras();
            if (extra != null) {
                btncaptimgpanic.setVisibility(View.GONE);
                imgpanic.setVisibility(View.VISIBLE);
                bitmap = (Bitmap) data.getExtras().get("data");
                imgpanic.setImageBitmap(bitmap);
            }
        }
    }

    @OnClick({R.id.btncaptimgpanic, R.id.btnsendpanic, R.id.imgpanic})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btncaptimgpanic:
                Intent intentbtn = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intentbtn, CAMERA_REQ_CODE);
                break;
            case R.id.imgpanic:
                Intent intentimg = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intentimg, CAMERA_REQ_CODE);
                break;
            case R.id.btnsendpanic:
                String encodeImage = null;
                if (bitmap != null){
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                    encodeImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                }
               // Toast.makeText(this, "spiner : " + input1.getSelectedItemsAsString().toString(), Toast.LENGTH_SHORT).show();
                InsertPanic(d_id, v_id, input1.getSelectedItemsAsString().toString(),edtpanicdetail.getText().toString(), encodeImage);
                break;
        }
    }

    public void InsertPanic(int did, int vid, String panictype, String panicdetail, String panicimg) {
        Log.d("Insertpanic", "InsertPanic: " + did + "," + vid + "," + panictype + "," + panicdetail);
        final ProgressDialog loading = ProgressDialog.show(this, "Cargando", "por favor espera...", false, false);
      //  SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
      //  final SharedPreferences.Editor editor = pref.edit();
        // OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create())
                // .client(httpClient.build())
                .build();
        SlimIface service = retrofit.create(SlimIface.class);
        Call<LogoutModel> call = service.AddPanic(did, vid, panictype, panicdetail, panicimg);
        call.enqueue(new Callback<LogoutModel>() {
            @Override
            public void onResponse(Call<LogoutModel> call, Response<LogoutModel> response) {

                if (response != null) {
                    Log.wtf("Transport insert trip", "Response: " + response.message());
                    if (response.body().error.equals(false)) {  //insert Success
                        Toast.makeText(Panic.this, "Insertado éxito", Toast.LENGTH_SHORT).show();
                        loading.dismiss();

                    } else // insert failure
                    {
                        loading.dismiss();
                        Toast.makeText(Panic.this, "Algo salió mal ", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    loading.dismiss();
                    Toast.makeText(Panic.this, response.body().toString(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<LogoutModel> call, Throwable t) {
                loading.dismiss();
                String merror = t.getMessage();
                Toast.makeText(Panic.this, merror, Toast.LENGTH_LONG).show();
                Log.wtf("Transport", "onFailure: " + t.getCause());
            }
        });
    }
}
