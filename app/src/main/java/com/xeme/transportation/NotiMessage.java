package com.xeme.transportation;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.xeme.transportation.Fragment.Frag_NotiAssignTrip;
import com.xeme.transportation.Fragment.Frag_NotiMsg;
import com.xeme.transportation.SlidingTab.PagerSlidingTabStrip;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.MySharedPreferences;
import com.xeme.transportation.utils.Utils;

public class NotiMessage extends AppCompatActivity {

    MySharedPreferences mypref;
    int driverid;

    private PagerSlidingTabStrip strip;
    private String[] strs = new String[]{"mensajes","Mensaje de viaje"};
    private ViewPager mPager;
    private StripAdapter stripAdapter;
    int txtSize = Utils.dpToPx(20);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noti_message);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);

        Log.d("00000000000000","hii");

        mypref = MySharedPreferences.getInstance(getApplicationContext(), "Transportation");
        driverid = mypref.getInt(Constants.KEY_DRIVER_ID,0);

        strip = (PagerSlidingTabStrip)findViewById(R.id.strip);
        mPager = (ViewPager)findViewById(R.id.pager);
       // mPager.setId(0x1000);
        System.out.println(strs);
        stripAdapter = new StripAdapter(getSupportFragmentManager(),strs);
        //adapter = new SimpleAdapter(getActivity().getSupportFragmentManager(), this.mWords, title);
        strip.setTextColor(Color.parseColor("#3F51B5"));
        strip.setTextSize(txtSize);
        strip.setIndicatorColor(Color.parseColor("#3F51B5"));
        strip.setIndicatorHeight(4);
        strip.setUnderlineHeight(1);
        strip.setShouldExpand(true);
        strip.setDividerColor(R.color.tab);
        // strip.setDividerColor(android.R.color.transparent);
        mPager.setAdapter(stripAdapter);
         strip.setViewPager(mPager);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    class StripAdapter extends FragmentStatePagerAdapter {
        private String mNewTitle;
        CharSequence Titles[];
        //private ArrayList<Word> mWords;

//        public StripAdapter(FragmentManager fm) {
//            super(fm);
//           // this.mWords = words;
//           // this.mNewTitle = newTitle;
//
//           // OnClickArrowsAdapter mListener;
//        }

        public StripAdapter(FragmentManager fragmentManager,CharSequence mTitles[]) {
            super(fragmentManager);
            this.Titles = mTitles;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0){
                Frag_NotiMsg fragnoti = new Frag_NotiMsg();
               // Bundle bundle = new Bundle();
               // TodayWord currentFragment = new TodayWord();
                //currentFragment.setOnTodaysBizzwordListener(new C12521());
               // bundle.putInt(Consts.INTENT_FRAGMENT, position);
               // bundle.putSerializable(Consts.INTENT_WORDS, mWords);
               // bundle.putString(Consts.INTENT_TITLE, mNewTitle);
               // currentFragment.setArguments(bundle);
               // return currentFragment;
//                TodayWord fragtoday = new TodayWord();
//                return fragtoday;
                return fragnoti;
//            }else if (position == 1){
//               // Frag_Theme fragFavorite = new Frag_Theme();
//                return null;// fragFavorite;
            }else {
                Frag_NotiAssignTrip fragweekly = new Frag_NotiAssignTrip();
                return fragweekly;
            }
        }

        @Override
        public int getCount() {
            return Titles.length;
        }
        @Override
        public CharSequence getPageTitle(int position) {
              return Titles[position];
            //return ((Word) this.mWords.get(position)).getWord();
        }
    }

//    public void GetSMSdata(){
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(Constants.url)
//                .addConverterFactory(GsonConverterFactory.create())
//                // .client(httpClient.build())
//                .build();
//
//        SlimIface service = retrofit.create(SlimIface.class);
//        System.out.println("0000000000" + driverid);Frag_NotiMsg
//        Call<smsdataresponse> call = service.getsmsdata(driverid);
//        System.out.println("1111111111" + call.toString());
//        call.enqueue(new Callback<smsdataresponse>() {
//            @Override
//            public void onResponse(Call<smsdataresponse> call, Response<smsdataresponse> response) {
//
//                if (response != null){
//                    Log.wtf("2222222222222", "Response: "+response.message());
//                    if (response.body().error.equals(false)) {
//                        System.out.println("2222222222" + response.toString());
//                        List<smsdata> refuelingdatas = response.body().getResults();
//                        System.out.println("3333333333 "+refuelingdatas.toString());
//
//                        // liststarttrip.setAdapter(new StarttripRecordsAdapter(getActivity(),R.layout.liststarttriprecords,refuelingdatas));
//                        smsAdapter adapter = new smsAdapter(getApplicationContext(), R.layout.listsinglerefuellingrecords, refuelingdatas);
//
//                        SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(adapter);
//                        swingBottomInAnimationAdapter.setAbsListView(listrefuelling);
//
//                        assert swingBottomInAnimationAdapter.getViewAnimator() != null;
//                        swingBottomInAnimationAdapter.getViewAnimator().setInitialDelayMillis(
//                                Constants.INITIAL_DELAY_MILLIS);
//
//                        listrefuelling.setAdapter(swingBottomInAnimationAdapter);
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<smsdataresponse> call, Throwable t) {
//                String merror = t.getMessage();
//                Toast.makeText(getApplicationContext(), merror, Toast.LENGTH_LONG).show();
//                Log.wtf("Transport", "onFailure: "+t.getCause());
//            }
//        });
//    }
}
