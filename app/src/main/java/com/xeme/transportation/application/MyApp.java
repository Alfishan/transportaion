package com.xeme.transportation.application;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.firebase.client.Firebase;

/**
 * Created by root on 25/6/16.
 */
public class MyApp extends Application {

    public MyApp() {
    }
    @Override
    public void onCreate() {
        super.onCreate();
       // MultiDex.install(this);
        Firebase.setAndroidContext(getApplicationContext());
       // FlowManager.init(this);
    }
    @Override
    public void onTerminate() {
        super.onTerminate();
        //FlowManager.destroy();
    }
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
