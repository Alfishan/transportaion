package com.xeme.transportation;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.xeme.transportation.DateTimePicker.SlideDateTimeListener;
import com.xeme.transportation.DateTimePicker.SlideDateTimePicker;
import com.xeme.transportation.Interface.SlimIface;
import com.xeme.transportation.POJO.GetdataBeforeStopTrip;
import com.xeme.transportation.POJO.StartDriverTripModel;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.MySharedPreferences;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StopTrip extends AppCompatActivity {
    Button btnSubmit;
    MySharedPreferences mypref;
    int cID, dID, tripID;
    @BindView(R.id.StoptripPackageImg)
    AppCompatImageView StoptripPackageImg;
    @BindView(R.id.StoptripPackagebtn)
    AppCompatButton StoptripPackagebtn;
    Bitmap bitmap;

    private SimpleDateFormat mFormatter = new SimpleDateFormat("dd MM yyyy hh:mm aa");
    EditText edtstopstartLocation, edtstopstartOdometer, edtstopstartDate, edtstopDestinationLocation, edtstopfinalodometer, edtstopenddate, edtstopNotes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stop_trip);
        ButterKnife.bind(this);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);

        mypref = MySharedPreferences.getInstance(this, "Transportation");
        cID = mypref.getInt(Constants.KEY_CUSTOMER_ID, 0);
        dID = mypref.getInt(Constants.KEY_DRIVER_ID, 0);
        tripID = mypref.getInt(Constants.KEY_TRIP_ID, 0);

        GetdataBeforeStopTrip(dID, cID, tripID);

        //   Toast.makeText(StopTrip.this, "tripidoncreate : " + tripID, Toast.LENGTH_SHORT).show();

//        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
//        final SharedPreferences.Editor editor = pref.edit();

        edtstopstartLocation = (EditText) findViewById(R.id.StoptripLocation);
        edtstopstartOdometer = (EditText) findViewById(R.id.StopstarttripOdometer);
        edtstopstartDate = (EditText) findViewById(R.id.StopstarttripDate);
        edtstopDestinationLocation = (EditText) findViewById(R.id.StoptripDestinationLocation);
        edtstopfinalodometer = (EditText) findViewById(R.id.StoptripOdometer);
        edtstopenddate = (EditText) findViewById(R.id.StoptripDate);
        edtstopNotes = (EditText) findViewById(R.id.StoptripExtranote);

        btnSubmit = (Button) findViewById(R.id.btn_Stoptripsubmit);

        edtstopenddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        //.setMinDate(minDate)
                        //.setMaxDate(maxDate)
                        //.setIs24HourTime(true)
                        //.setTheme(SlideDateTimePicker.HOLO_DARK)
                        //.setIndicatorColor(Color.parseColor("#990000"))
                        .build()
                        .show();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String encodeImage = null;
                if (bitmap != null){
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                    encodeImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                }

                if (CheckFieldValidation()) {
                    String stopfinalodometer = edtstopfinalodometer.getText().toString();
                    String stopdate = edtstopenddate.getText().toString();
                    String stopnotes = edtstopNotes.getText().toString();
                    int isstart = 0;

                    StopDriverTrip(tripID, dID, cID, stopfinalodometer, stopdate, encodeImage, stopnotes, isstart);
                }
            }
        });
    }

    private boolean CheckFieldValidation() {
        boolean valid = true;
        if (edtstopfinalodometer.getText().toString().equals("")) {
            edtstopfinalodometer.setError("no puede estar vacía");
            valid = false;
        } else if (edtstopNotes.getText().toString().equals("")) {
            edtstopNotes.setError("no puede estar vacía");
            valid = false;
        } else if (edtstopenddate.getText().toString().equals("")) {
            edtstopenddate.setError("no puede estar vacía");
            valid = false;
        }else if (bitmap == null){
            StoptripPackagebtn.setError("Por favor captura de imágenes");
            valid = false;
        }
        return valid;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void GetdataBeforeStopTrip(int did, int cid, int tripid) {

        final ProgressDialog loading = ProgressDialog.show(this, "Cargando", "por favor espera...", false, false);

        // OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create())
                // .client(httpClient.build())
                .build();

        SlimIface service = retrofit.create(SlimIface.class);
        Call<GetdataBeforeStopTrip> call = service.GetforStop(did, cid, tripid);
        call.enqueue(new Callback<GetdataBeforeStopTrip>() {
            @Override
            public void onResponse(Call<GetdataBeforeStopTrip> call, Response<GetdataBeforeStopTrip> response) {

                if (response != null) {
                    Log.wtf("Transport insert trip", "Response: " + response.message());
                    if (response.body().error.equals(false)) {  //Fatch Success
                        edtstopstartLocation.setText(response.body().startlocation.toString());
                        edtstopstartOdometer.setText(response.body().initialodometer.toString());
                        edtstopstartDate.setText(response.body().dateonstart.toString());
                        edtstopDestinationLocation.setText(response.body().destinationlocation.toString());

                        loading.dismiss();

                    } else // Fatch failure
                    {
                        loading.dismiss();
                        //Toast.makeText(StopTrip.this, " ----------", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    loading.dismiss();
                    Toast.makeText(StopTrip.this, response.body().toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetdataBeforeStopTrip> call, Throwable t) {
                loading.dismiss();
                String merror = t.getMessage();
                Toast.makeText(StopTrip.this, merror, Toast.LENGTH_LONG).show();
                Log.wtf("Transport", "onFailure: " + t.getCause());
            }
        });
    }

    private SlideDateTimeListener listener = new SlideDateTimeListener() {

        @Override
        public void onDateTimeSet(Date date) {
            edtstopenddate.setText(mFormatter.format(date));
            //Toast.makeText(StopTrip.this,mFormatter.format(date), Toast.LENGTH_SHORT).show();
        }

        // Optional cancel listener
        @Override
        public void onDateTimeCancel() {
            Toast.makeText(StopTrip.this,
                    "Cancelado", Toast.LENGTH_SHORT).show();
        }
    };


    public void StopDriverTrip(int tripid, int did, int cid, String finalodometer, String enddate, String packageimg, String extranotesonstop, int isstart) {

        final ProgressDialog loading = ProgressDialog.show(this, "Cargando", "por favor espera...", false, false);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        final SharedPreferences.Editor editor = pref.edit();

        // OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create())
                // .client(httpClient.build())
                .build();

        SlimIface service = retrofit.create(SlimIface.class);
        Call<StartDriverTripModel> call = service.StopDriverTrip(tripid, did, cid, finalodometer, enddate, packageimg, extranotesonstop, isstart);
        call.enqueue(new Callback<StartDriverTripModel>() {
            @Override
            public void onResponse(Call<StartDriverTripModel> call, Response<StartDriverTripModel> response) {

                if (response != null) {
                    Log.wtf("Transport stop trip", "Response: " + response.message());
                    if (response.body().error.equals(false)) {  //insert Success
                        loading.dismiss();
                        Toast.makeText(StopTrip.this, "Viaje terminó con éxito", Toast.LENGTH_SHORT).show();
                        mypref.putInt(Constants.KEY_TRIP_ID, 0);
                        mypref.commit();
                        int t = mypref.getInt(Constants.KEY_TRIP_ID, 0);

                        editor.putInt("Toggle", 0);
                        mypref.putBoolean(Constants.IsTripStarted, false);
                        mypref.commit();
                        editor.commit();

                        // Toast.makeText(StopTrip.this, "tripidonclick : " + t , Toast.LENGTH_LONG).show();
                        // onBackPressed();

                        Intent i = new Intent(StopTrip.this, Drawer.class);
                        startActivity(i);
                        StopTrip.this.finish();
                        loading.dismiss();

                    } else // insert failure
                    {
                        loading.dismiss();
                        Toast.makeText(StopTrip.this, "Por favor, insertar datos correcta ", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    loading.dismiss();
                    Toast.makeText(StopTrip.this, response.body().toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StartDriverTripModel> call, Throwable t) {
                loading.dismiss();
                String merror = t.getMessage();
                Toast.makeText(StopTrip.this, merror, Toast.LENGTH_LONG).show();
                Log.wtf("Transport", "onFailure: " + t.getCause());
            }
        });
    }

    @OnClick({R.id.StoptripPackageImg, R.id.StoptripPackagebtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.StoptripPackageImg:
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 1);
                break;
            case R.id.StoptripPackagebtn:
                Intent intentbtn = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intentbtn, 1);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bundle extra = data.getExtras();
        if (extra != null) {
            StoptripPackagebtn.setVisibility(View.GONE);
            StoptripPackageImg.setVisibility(View.VISIBLE);
            bitmap = (Bitmap) data.getExtras().get("data");
            StoptripPackageImg.setImageBitmap(bitmap);
        }
    }
}
