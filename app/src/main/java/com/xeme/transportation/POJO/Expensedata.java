package com.xeme.transportation.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 2/8/16.
 */
public class Expensedata {

    @SerializedName("date_time")
    @Expose
    public String date_time;

    @SerializedName("odometer")
    @Expose
    public String odometer;

    public String getNameofexpense() {
        return nameofexpense;
    }

    public void setNameofexpense(String nameofexpense) {
        this.nameofexpense = nameofexpense;
    }

    @SerializedName("name_of_expenses")
    @Expose
    public String nameofexpense;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @SerializedName("value")
    @Expose
    public String value;

    @SerializedName("latitude")
    @Expose
    public String latitude;

    @SerializedName("longitude")
    @Expose
    public String longitude;

    public String getOdometer() {
        return odometer;
    }

    public void setOdometer(String odometer) {
        this.odometer = odometer;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

}
