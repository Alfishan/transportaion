package com.xeme.transportation.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 7/10/16.
 */

public class ReqIMEI {
        @SerializedName("error")
        @Expose
        public Boolean error;

        @SerializedName("message")
        @Expose
        public String message;
}
