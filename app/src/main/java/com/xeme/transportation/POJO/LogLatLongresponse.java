package com.xeme.transportation.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 5/8/16.
 */
public class LogLatLongresponse {

    @SerializedName("error")
    @Expose
    public Boolean error;

    @SerializedName("startloc")
    @Expose
    public String startloc;

    @SerializedName("destnationloc")
    @Expose
    public String endloc;

    @SerializedName("results")
    private List<LogsLatLongdata> results;


    public List<LogsLatLongdata> getResults() {
        return results;
    }

    public void setResults(List<LogsLatLongdata> results) {
        this.results = results;
    }

}
