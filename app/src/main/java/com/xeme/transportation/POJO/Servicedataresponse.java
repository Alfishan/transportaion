package com.xeme.transportation.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/8/16.
 */
public class Servicedataresponse {

    @SerializedName("error")
    @Expose
    public Boolean error;

    @SerializedName("results")
    private List<Servicedata> results;


    public List<Servicedata> getResults() {
        return results;
    }

    public void setResults(List<Servicedata> results) {
        this.results = results;
    }

}
