package com.xeme.transportation.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/8/16.
 */
public class TripLogsresponse {
    @SerializedName("error")
    @Expose
    public Boolean error;

    @SerializedName("results")
    private List<TripLogsdata> results;


    public List<TripLogsdata> getResults() {
        return results;
    }

    public void setResults(List<TripLogsdata> results) {
        this.results = results;
    }
}
