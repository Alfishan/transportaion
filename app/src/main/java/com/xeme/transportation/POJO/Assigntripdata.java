package com.xeme.transportation.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 4/8/16.
 */
public class Assigntripdata {

    @SerializedName("start")
    @Expose
    public String start;

    @SerializedName("end")
    @Expose
    public String end;

    @SerializedName("message")
    @Expose
    public String messege;

    @SerializedName("date_time")
    @Expose
    public String date_time;

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getMessege() {
        return messege;
    }

    public void setMessege(String messege) {
        this.messege = messege;
    }


}
