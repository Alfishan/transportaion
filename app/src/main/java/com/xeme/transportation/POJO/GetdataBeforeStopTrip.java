package com.xeme.transportation.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 9/7/16.
 */
public class GetdataBeforeStopTrip {

    @SerializedName("error")
    @Expose public Boolean error;

    @SerializedName("start_location")
    @Expose
    public String startlocation;

    @SerializedName("initial_odometer")
    @Expose
    public String initialodometer;

    @SerializedName("date_time_onstart")
    @Expose
    public String dateonstart;

    @SerializedName("destination_location")
    @Expose
    public String destinationlocation;

}
