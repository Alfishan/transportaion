package com.xeme.transportation.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 7/7/16.
 */
public class StartDriverTripModel {


        @SerializedName("error")
        @Expose
        public Boolean error;

        @SerializedName("tripid")
        @Expose
        public Integer tripid;

}
