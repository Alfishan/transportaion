package com.xeme.transportation.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 5/8/16.
 */
public class LogsLatLongdata {

    @SerializedName("date_time")
    @Expose
    public String dateTime;
    @SerializedName("category")
    @Expose
    public String category;
    @SerializedName("latitude")
    @Expose
    public String latitude;
    @SerializedName("longitude")
    @Expose
    public String longitude;
}
