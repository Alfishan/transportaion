package com.xeme.transportation.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 2/7/16.
 */


public class LoginModel {

    @SerializedName("error")
    @Expose
    public Boolean error;
    @SerializedName("match")
    @Expose
    public Boolean match;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("apikey")
    @Expose
    public String apikey;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("did")
    @Expose
    public Integer did;
    @SerializedName("cid")
    @Expose
    public Integer cid;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("vid")
    @Expose
    public String vid;
    @SerializedName("vmodel")
    @Expose
    public String vmodel;
    @SerializedName("vimg")
    @Expose
    public String vimage;
    @SerializedName("brand")
    @Expose
    public String vbrand;
    @SerializedName("adminmailid")
    @Expose
    public String adminmail;
}

