package com.xeme.transportation.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 16/7/16.
 */
public class Starttripdata {

    @SerializedName("start_location")
    @Expose
    public String startlocation;

    @SerializedName("initial_odometer")
    @Expose
    public String initialodometer;

    @SerializedName("date_time_onstart")
    @Expose
    public String datetimeonstart;

    @SerializedName("destination_location")
    @Expose
    public String destinationlocation;

//    int did;
//    int cid;
//    String reason;
//    String extranotesonstart;
//    int istart;
//
   public String getStartlocation() {
       return startlocation;
   }

    public void setStartlocation(String startlocation) {
        this.startlocation = startlocation;
    }
//
//    public int getDid() {
//        return did;
//    }
//
//    public void setDid(int did) {
//        this.did = did;
//    }
//
//    public int getCid() {
//        return cid;
//    }
//
//    public void setCid(int cid) {
//        this.cid = cid;
//    }
//
    public String getInitialodometer() {
       return initialodometer;
    }

    public void setInitialodometer(String initialodometer) {
        this.initialodometer = initialodometer;
    }

    public String getDatetimeonstart() {
        return datetimeonstart;
    }

    public void setDatetimeonstart(String datetimeonstart) {
        this.datetimeonstart = datetimeonstart;
    }

    public String getDestinationlocation() {
        return destinationlocation;
    }
//
//    public void setDestinationlocation(String destinationlocation) {
//        this.destinationlocation = destinationlocation;
//    }
//
//    public String getReason() {
//        return reason;
//    }
//
//    public void setReason(String reason) {
//        this.reason = reason;
//    }
//
//    public String getExtranotesonstart() {
//        return extranotesonstart;
//    }
//
//    public void setExtranotesonstart(String extranotesonstart) {
//        this.extranotesonstart = extranotesonstart;
//    }
//
//    public int getIstart() {
//        return istart;
//    }
//
//    public void setIstart(int istart) {
//        this.istart = istart;
//    }
}

