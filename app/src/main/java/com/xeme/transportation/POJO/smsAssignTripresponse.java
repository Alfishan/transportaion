package com.xeme.transportation.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 4/8/16.
 */
public class smsAssignTripresponse {
    @SerializedName("error")
    @Expose
    public Boolean error;

//    @SerializedName("message")
//    @Expose
//    public String message;

    @SerializedName("results")
    private List<Assigntripdata> results;

    public List<Assigntripdata> getResults() {
        return results;
    }

    public void setResults(List<Assigntripdata> results) {
        this.results = results;
    }
}
