package com.xeme.transportation.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/8/16.
 */
public class Refuelingdataresponse {
    @SerializedName("error")
    @Expose
    public Boolean error;

    @SerializedName("results")
    private List<Refuelingdata> results;


    public List<Refuelingdata> getResults() {
        return results;
    }

    public void setResults(List<Refuelingdata> results) {
        this.results = results;
    }
}
