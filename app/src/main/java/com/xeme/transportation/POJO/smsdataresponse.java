package com.xeme.transportation.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/8/16.
 */
public class smsdataresponse {
    @SerializedName("error")
    @Expose
    public Boolean error;

//    @SerializedName("message")
//    @Expose
//    public String message;

    @SerializedName("results")
    private List<smsdata> results;

    public List<smsdata> getResults() {
        return results;
    }

    public void setResults(List<smsdata> results) {
        this.results = results;
    }
}
