package com.xeme.transportation.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/8/16.
 */
public class Expensedataresponse {

    @SerializedName("error")
    @Expose
    public Boolean error;

    @SerializedName("results")
    private List<Expensedata> results;


    public List<Expensedata> getResults() {
        return results;
    }

    public void setResults(List<Expensedata> results) {
        this.results = results;
    }
}
