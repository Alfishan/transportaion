package com.xeme.transportation.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 16/7/16.
 */
public class Starttripdataresponse {

    @SerializedName("error")
    @Expose
    public Boolean error;

    @SerializedName("results")
    private List<Starttripdata> results;


    public List<Starttripdata> getResults() {
        return results;
    }

    public void setResults(List<Starttripdata> results) {
        this.results = results;
    }
}
