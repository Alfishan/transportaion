package com.xeme.transportation.Bean;

/**
 * Created by root on 24/5/16.
 */
public class RowItem {
    //private int imageId;
    private String fueltype;
    private String fuelprise;
    private String odometer;
    private String date;
    private String fuellitter;
    private String totalprice;

    public RowItem(String fueltype, String fuelprise,String odometer ,String date, String fuellitter,String totalprice) {
       // this.imageId = imageId;
        this.fueltype = fueltype;
        this.fuelprise = fuelprise;
        this.odometer = odometer;
        this.date = date;
        this.fuellitter = fuellitter;
        this.totalprice = totalprice;
    }
//    public int getImageId() {
//        return imageId;
//    }
//    public void setImageId(int imageId) {
//        this.imageId = imageId;
//    }
    public String getFueltype() {
        return fueltype;
    }
    public void setFueltype(String fueltype) {
        this.fueltype = fueltype;
    }

    public String getFuelprise() {
        return fuelprise;
    }
    public void setFuelprise(String fuelprise) {
        this.fuelprise = fuelprise;
    }

    public String getOdometer() {
        return odometer;
    }
    public void setOdometer(String odometer) {
        this.odometer = odometer;
    }

    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }

    public String getFuellitter() {
        return fuellitter;
    }
    public void setFuellitter(String fuellitter) {
        this.fuellitter = fuellitter;
    }

    public String getTotalprice() {
        return totalprice;
    }
    public void setTotalprice(String totalprice) {
        this.totalprice = totalprice;
    }

//    @Override
//    public String toString() {
//        return fueltype + "\n" + desc;
//    }
}
