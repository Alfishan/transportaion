package com.xeme.transportation.Bean;

/**
 * Created by root on 24/5/16.
 */
public class ExRowItem {
    //private int imageId;
    private String expensetye;
    private String odometer;
    private String date;
    private String totalprice;

    public ExRowItem(String expensetye, String odometer ,String date, String totalprice) {
        // this.imageId = imageId;
        this.expensetye = expensetye;

        this.odometer = odometer;
        this.date = date;

        this.totalprice = totalprice;
    }
    //    public int getImageId() {
//        return imageId;
//    }
//    public void setImageId(int imageId) {
//        this.imageId = imageId;
//    }
    public String getExpensetye() {
        return expensetye;
    }
    public void setExpensetye(String fueltype) {
        this.expensetye = fueltype;
    }

    public String getOdometer() {
        return odometer;
    }
    public void setOdometer(String odometer) {
        this.odometer = odometer;
    }

    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }

    public String getTotalprice() {
        return totalprice;
    }
    public void setTotalprice(String totalprice) {
        this.totalprice = totalprice;
    }

//    @Override
//    public String toString() {
//        return fueltype + "\n" + desc;
//    }
}
