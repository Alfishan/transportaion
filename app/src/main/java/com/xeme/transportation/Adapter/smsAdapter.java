package com.xeme.transportation.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.xeme.transportation.POJO.smsdata;
import com.xeme.transportation.R;

import java.util.List;

/**
 * Created by root on 2/8/16.
 */
public class smsAdapter extends ArrayAdapter<smsdata> {

    Context context;

    public smsAdapter(Context context, int resourceId,
                              List<smsdata> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        //ImageView imageView;
        TextView txtmsg,txtdate;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        smsdata rowItem = getItem(position);
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listnotimsg, null);
            holder = new ViewHolder();

            // holder.txttrip = (TextView) convertView.findViewById(R.id.txttripid);
            holder.txtdate = (TextView) convertView.findViewById(R.id.txtmsgdate);
            holder.txtmsg = (TextView) convertView.findViewById(R.id.txtmsg);
            //  holder.txtextra = (TextView) convertView.findViewById(R.id.txtextranotes);
            // holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
            //  holder.txtstart.setText(rowItem.getStartlocation());
            // holder.txtodometer.setText(rowItem.getInitialodometer());
            // holder.txtdate.setText(rowItem.getDatetimeonstart());
            //  holder.txtdestination.setText(rowItem.getDestinationlocation());
            // holder.txtextra.setText(rowItem.getExtranotesonstart());
        }
        holder.txtdate.setText(String.valueOf(rowItem.getDate_time()));
        holder.txtmsg.setText(rowItem.getMessege());
        return convertView;
    }
}
