package com.xeme.transportation.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.xeme.transportation.POJO.Servicedata;
import com.xeme.transportation.R;

import java.util.List;

/**
 * Created by root on 2/8/16.
 */
public class ServicedataAdapter extends ArrayAdapter<Servicedata> {

    Context context;

    public ServicedataAdapter(Context context, int resourceId,
                                   List<Servicedata> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        //ImageView imageView;
        TextView txttype,txtodometer,txtdate,txtvalue,txttrip;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        Servicedata rowItem = getItem(position);
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listservicedata, null);
            holder = new ViewHolder();

            // holder.txttrip = (TextView) convertView.findViewById(R.id.txttripid);
         //   holder.txtstart = (TextView) convertView.findViewById(R.id.txtstarttrip);
            holder.txtodometer = (TextView) convertView.findViewById(R.id.txtserviceinitialodometer);
            holder.txtdate = (TextView) convertView.findViewById(R.id.txtservicedate);
            holder.txttype = (TextView) convertView.findViewById(R.id.txtservicetype);
            holder.txtvalue = (TextView) convertView.findViewById(R.id.txtservicevalue);
            //  holder.txtextra = (TextView) convertView.findViewById(R.id.txtextranotes);
            // holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();

        }
        holder.txtdate.setText(rowItem.getDate_time());
        holder.txtodometer.setText(rowItem.getOdometer());
        holder.txttype.setText(rowItem.getSrvicetype());
        holder.txtvalue.setText(rowItem.getSrvicetype());
        return convertView;
    }
}
