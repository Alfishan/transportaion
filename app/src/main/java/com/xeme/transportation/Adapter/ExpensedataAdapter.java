package com.xeme.transportation.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.xeme.transportation.POJO.Expensedata;
import com.xeme.transportation.R;

import java.util.List;

/**
 * Created by root on 2/8/16.
 */
public class ExpensedataAdapter  extends ArrayAdapter<Expensedata> {

    Context context;

    public ExpensedataAdapter(Context context, int resourceId,
                              List<Expensedata> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        //ImageView imageView;
        TextView txttype,txtvalue,txtodometer,txtdate,txtextra,txttrip;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        Expensedata rowItem = getItem(position);
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listexpenserecords, null);
            holder = new ViewHolder();

            // holder.txttrip = (TextView) convertView.findViewById(R.id.txttripid);
            holder.txtodometer = (TextView) convertView.findViewById(R.id.txtexpenseodometer);
            holder.txtdate = (TextView) convertView.findViewById(R.id.txtexpensedate);
            holder.txttype = (TextView) convertView.findViewById(R.id.txtexpensetype);
            holder.txtvalue = (TextView) convertView.findViewById(R.id.txtexpensevalue);
            //  holder.txtextra = (TextView) convertView.findViewById(R.id.txtextranotes);
            // holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtdate.setText(rowItem.getDate_time());
        holder.txtodometer.setText(rowItem.getOdometer());
        holder.txttype.setText(rowItem.getNameofexpense());
        holder.txtvalue.setText(rowItem.getValue());
        return convertView;
    }
}

