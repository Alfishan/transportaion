package com.xeme.transportation.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.xeme.transportation.POJO.Assigntripdata;
import com.xeme.transportation.R;
import com.xeme.transportation.StartTrip;
import com.xeme.transportation.StopTrip;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.MySharedPreferences;

import java.util.List;

/**
 * Created by root on 4/8/16.
 */
public class AssigntripAdapter extends ArrayAdapter<Assigntripdata> {

    Context context;
    MySharedPreferences mypref;

    public AssigntripAdapter(Context context, int resourceId,
                      List<Assigntripdata> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        //ImageView imageView;
        TextView txtmsg,txtdate,txtstart,txtend;
        TableRow tblrow;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        mypref = MySharedPreferences.getInstance(context,"Transportation");

        final Assigntripdata rowItem = getItem(position);
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listnotiassigntrip, null);
            holder = new ViewHolder();

            holder.tblrow = (TableRow)convertView.findViewById(R.id.tblassigntriprow);

            // holder.txttrip = (TextView) convertView.findViewById(R.id.txttripid);
            holder.txtdate = (TextView) convertView.findViewById(R.id.txtmsgtripdate);
            holder.txtmsg = (TextView) convertView.findViewById(R.id.txtmsgtripmsg);
            holder.txtstart = (TextView) convertView.findViewById(R.id.txtmsgtripstart);
            holder.txtend = (TextView) convertView.findViewById(R.id.txtmsgtripend);
            //  holder.txtextra = (TextView) convertView.findViewById(R.id.txtextranotes);
            // holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
            //  holder.txtstart.setText(rowItem.getStartlocation());
            // holder.txtodometer.setText(rowItem.getInitialodometer());
            // holder.txtdate.setText(rowItem.getDatetimeonstart());
            //  holder.txtdestination.setText(rowItem.getDestinationlocation());
            // holder.txtextra.setText(rowItem.getExtranotesonstart());
        }
        holder.txtdate.setText(String.valueOf(rowItem.getDate_time()));
        holder.txtmsg.setText(String.valueOf(rowItem.getMessege()));
        holder.txtstart.setText(String.valueOf(rowItem.getStart()));
        holder.txtend.setText(String.valueOf(rowItem.getEnd()));

        holder.tblrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(getContext(), rowItem.getMessege().toString(), Toast.LENGTH_SHORT).show();
                Boolean isTripStart = mypref.getBoolean(Constants.IsTripStarted, false);
              //  Toast.makeText(context, String.valueOf(isTripStart), Toast.LENGTH_LONG).show();
                if (isTripStart){
                    Toast.makeText(context, "Otro viaje ya está en curso ...\n completa por primera vez", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getContext(), StopTrip.class);
                    getContext().startActivity(intent);
                }else{
                    Intent intent = new Intent(getContext(), StartTrip.class);
                    intent.putExtra("starttriploc",rowItem.getStart());
                    intent.putExtra("endtriploc",rowItem.getEnd());
                    getContext().startActivity(intent);
                }
            }
        });
        return convertView;
    }
}
