package com.xeme.transportation.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.xeme.transportation.POJO.TripLogsdata;
import com.xeme.transportation.R;

import java.util.List;

/**
 * Created by root on 2/8/16.
 */
public class TripLogsAdapter extends ArrayAdapter<TripLogsdata> {

    private static final String TAG = "Mydata";
    Context context;

public TripLogsAdapter(Context context, int resourceId,
                       List<TripLogsdata> items) {
        super(context, resourceId, items);
        this.context = context;
        }

/*private view holder class*/
private class ViewHolder {
    //ImageView imageView;
    TextView txtdate,txtcategory,txtodometer,txtextra,txttrip,txtdestination;
}

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        TripLogsdata rowItem = getItem(position);
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_timeline, null);
            holder = new ViewHolder();
            // holder.txttrip = (TextView) convertView.findViewById(R.id.txttripid);
            holder.txtdate = (TextView) convertView.findViewById(R.id.txtlogdate);
            holder.txtodometer = (TextView) convertView.findViewById(R.id.txtlogodometer);
            holder.txtcategory = (TextView) convertView.findViewById(R.id.txtlogcategory);

            //  holder.txtdestination = (TextView) convertView.findViewById(R.id.txtdestinationlocation);
            // holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
         //  holder.txtdestination.setText(rowItem.getDestinationlocation());
            // holder.txtextra.setText(rowItem.getExtranotesonstart());
        }
        holder.txtdate.setText(rowItem.getDate_time());
        holder.txtcategory.setText(rowItem.getCategory());
        holder.txtodometer.setText(rowItem.getOdometer());
        //Log.w(TAG, "getView: " +rowItem.getDate_time());

        return convertView;
    }
}