package com.xeme.transportation.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.xeme.transportation.POJO.Starttripdata;
import com.xeme.transportation.R;

import java.util.List;

/**
 * Created by root on 24/5/16.
 */
public class StarttripRecordsAdapter extends ArrayAdapter<Starttripdata> {

    Context context;

    public StarttripRecordsAdapter(Context context, int resourceId,
                                   List<Starttripdata> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        //ImageView imageView;
        TextView txtstart,txtdestination,txtodometer,txtdate,txtextra,txttrip;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        Starttripdata rowItem = getItem(position);
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.liststarttriprecords, null);
            holder = new ViewHolder();

           // holder.txttrip = (TextView) convertView.findViewById(R.id.txttripid);
            holder.txtstart = (TextView) convertView.findViewById(R.id.txtstarttrip);
            holder.txtodometer = (TextView) convertView.findViewById(R.id.txtinitialodometer);
            holder.txtdate = (TextView) convertView.findViewById(R.id.txtdatetimeonstart);
            holder.txtdestination = (TextView) convertView.findViewById(R.id.txtdestinationlocation);
          //  holder.txtextra = (TextView) convertView.findViewById(R.id.txtextranotes);
            // holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();

           // holder.txtextra.setText(rowItem.getExtranotesonstart());
        }
        holder.txtstart.setText(rowItem.getStartlocation());
        holder.txtodometer.setText(rowItem.getInitialodometer());
        holder.txtdate.setText(rowItem.getDatetimeonstart());
        holder.txtdestination.setText(rowItem.getDestinationlocation());

        return convertView;
    }
}