package com.xeme.transportation.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.xeme.transportation.POJO.Refuelingdata;
import com.xeme.transportation.R;

import java.util.List;

/**
 * Created by root on 24/5/16.
 */
public class RefuellingRecordsAdapter extends ArrayAdapter<Refuelingdata> {

    Context context;

    public RefuellingRecordsAdapter(Context context, int resourceId,
                              List<Refuelingdata> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        //ImageView imageView;
        TextView txttype,txttotalprice,txtodometer,txtdate,txtextra,txttrip;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        Refuelingdata rowItem = getItem(position);
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listsinglerefuellingrecords, null);
            holder = new ViewHolder();

            // holder.txttrip = (TextView) convertView.findViewById(R.id.txttripid);
            holder.txttype = (TextView) convertView.findViewById(R.id.txtrefuelingfueltype);
            holder.txtodometer = (TextView) convertView.findViewById(R.id.txtrefuelingodometer);
            holder.txtdate = (TextView) convertView.findViewById(R.id.txtrefuelingdate);
            holder.txttotalprice = (TextView) convertView.findViewById(R.id.txtrefuelingtotalprice);
            //  holder.txtextra = (TextView) convertView.findViewById(R.id.txtextranotes);
            // holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txttype.setText(rowItem.getFueltype());
        holder.txtodometer.setText(rowItem.getOdometer());
        holder.txtdate.setText(rowItem.getDate_time());
        holder.txttotalprice.setText(rowItem.getTotalprice());
        return convertView;
    }
}
