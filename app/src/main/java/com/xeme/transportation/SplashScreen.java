package com.xeme.transportation;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.WindowManager;
import android.widget.Toast;

import com.afollestad.assent.Assent;
import com.afollestad.assent.AssentActivity;
import com.afollestad.assent.AssentCallback;
import com.afollestad.assent.PermissionResultSet;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.xeme.transportation.services.NotificationListener;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.MyLocationService;
import com.xeme.transportation.utils.MySharedPreferences;

import java.util.HashMap;
import java.util.Map;

public class SplashScreen extends AssentActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    MySharedPreferences mypref;
    int islogin;
   /* private My_Helper.NetworkChangeReceiver receiver;
    private DatabaseReference mDatabase;
    Firebase ref;
    My_Helper my_helper;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mypref = MySharedPreferences.getInstance(this, "Transportation");
        islogin = mypref.getInt(Constants.KEY_ISLOGIN, 0);


        //   Firebase.setAndroidContext(this);
        //    this.ref = new Firebase(Consta.FIREBASE_USER);

      /*     my_helper = new My_Helper(SplashScreen.this);
        mDatabase = FirebaseDatabase.getInstance().getReference();
      */
        if (checkPlayServices()) {
            init();
            if (isRegistered()) {
                startService(new Intent(this, NotificationListener.class));
                Intent Serviceintent = new Intent(this, MyLocationService.class);
                startService(Serviceintent);
            } else {
                //  Toast.makeText(SplashScreen.this, "Device not registred......", Toast.LENGTH_LONG).show();
                registerDevice();
                startService(new Intent(this, NotificationListener.class));
            }
        }

    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                //Logwtf("This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    void init() {
        if (!Assent.isPermissionGranted(Assent.ACCESS_FINE_LOCATION)) {
            // The if statement checks if the permission has already been granted before
            Assent.requestPermissions(new AssentCallback() {
                @Override
                public void onPermissionResult(PermissionResultSet result) {
                    if (result.allPermissionsGranted()) {
                        int SPLASH_TIME_OUT = 2000;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
//                                Intent intent=new Intent("android.location.GPS_ENABLED_CHANGE");
//                                intent.putExtra("enabled", true);
//                                sendBroadcast(intent);

                                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                //intent.putExtra("enabled", true);
                                //sendBroadcast(intent);
                                startActivity(intent);
                                //startActivity(SplashScreen.this,

                                if (islogin == 0) {
                                    startActivity(new Intent(SplashScreen.this, Login.class));
                                    SplashScreen.this.finish();
                                    // close this activity
                                } else {
                                    startActivity(new Intent(SplashScreen.this, Drawer.class));
                                    SplashScreen.this.finish();
                                    // close this activity
                                }

                            }
                        }, SPLASH_TIME_OUT);
                    } else {
                        init();
                        Toast.makeText(SplashScreen.this, "Debe conceder este permiso para utilizar esta aplicación", Toast.LENGTH_SHORT).show();
                    }
                    // Permission granted or denied
                }
            }, 69, Assent.ACCESS_FINE_LOCATION);
        } else {
            int SPLASH_TIME_OUT = 2000;
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (islogin == 0) {
                        startActivity(new Intent(SplashScreen.this, Login.class));
                        SplashScreen.this.finish();
                        // close this activity
                    } else {
                        startActivity(new Intent(SplashScreen.this, Drawer.class));
                        SplashScreen.this.finish();
                        // close this activity
                    }
                }
            }, SPLASH_TIME_OUT);

        }

    }

   /* void registerNSL() {

        // Registering Network state listener
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new My_Helper.NetworkChangeReceiver();
        registerReceiver(receiver, filter);
    }*/


    @Override
    protected void onDestroy() {
        //Logwtf("onDestory");
        super.onDestroy();
        //  unregisterReceiver(receiver);

    }

    private boolean isRegistered() {
        return mypref.getBoolean(Constants.REGISTERED, false);
    }

    private void registerDevice() {
        //Creating a firebase object

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference("sms").push();



/*
        Firebase firebase = new Firebase(Constants.FIREBASE_APP);

        //Pushing a new element to firebase it will automatically create a unique id
        Firebase newFirebase = firebase.push();*/

        //Creating a map to store name value pair
        Map<String, String> val = new HashMap<>();

        //pushing msg = none in the map
        val.put("msg", "none");
        val.put("id", "none");

        //saving the map to firebase
        //  newFirebase.setValue(val);

        myRef.setValue(val);

        //Getting the unique id generated at firebase
        String uniqueId = myRef.getKey();
        // Log.d("999999999999",uniqueId);
        mypref.putString(Constants.FirebaseID, uniqueId);
        mypref.putBoolean(Constants.REGISTERED, true);
        mypref.commit();
        String firebaseID = mypref.getString(Constants.FirebaseID, "");
        //  Toast.makeText(SplashScreen.this, "Firebase ID : " + firebaseID, Toast.LENGTH_SHORT).show();
        //Finally we need to implement a method to store this unique id to our server
        // sendIdToServer(uniqueId);
    }

}
