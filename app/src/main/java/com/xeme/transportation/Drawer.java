package com.xeme.transportation;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.xeme.transportation.Fragment.Frag_Dashboard;
import com.xeme.transportation.Fragment.Frag_ExpenseRecords;
import com.xeme.transportation.Fragment.Frag_GPSLogsRecords;
import com.xeme.transportation.Fragment.Frag_RefuellingRecords;
import com.xeme.transportation.Fragment.Frag_Servicerecords;
import com.xeme.transportation.Fragment.Frag_StartTripRecords;
import com.xeme.transportation.Interface.SlimIface;
import com.xeme.transportation.POJO.LogoutModel;
import com.xeme.transportation.utils.CircleTransformNew;
import com.xeme.transportation.utils.Constants;
import com.xeme.transportation.utils.Gpslocation;
import com.xeme.transportation.utils.MySharedPreferences;
import com.xeme.transportation.utils.Utils;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Drawer extends ActionBarActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    FragmentManager fm = getFragmentManager();
    Fragment frag = null;
    Menu menu;
    String name = "FSG";
    String VEHICLE_BRAND;
    int cid,did;

   // LinearLayout addvehicle;
    MenuItem tripbutton;
    MySharedPreferences mypref;
    AppCompatImageView Profile_Pic,Vehicle_Pic;
    AppCompatTextView Profile_Vehical_Name;
   int imgSize = Utils.dpToPx(150);
    String ProfilePath,VImagePath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mypref = MySharedPreferences.getInstance(this,"Transportation");
        String name = mypref.getString(Constants.KEY_DRIVER_NAME,"FSG");
        VEHICLE_BRAND = mypref.getString(Constants.KEY_VEHICLE_BRAND,"");
        int vid = mypref.getInt(Constants.KEY_VEHICLE_ID,0);
        cid = mypref.getInt(Constants.KEY_CUSTOMER_ID,0);
        did = mypref.getInt(Constants.KEY_DRIVER_ID,0);

        setContentView(R.layout.activity_drawer);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View navhead=navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
        menu = navigationView.getMenu();
        ProfilePath = mypref.getString(Constants.KEY_DRIVER_IMAGE,"");
        VImagePath = mypref.getString(Constants.KEY_VEHICLE_IMAGE,"");
        //String imei = mypref.getString(Constants.IMEI,"");

//        ActionBar actionbar = getSupportActionBar();
//        actionbar.setTitle("hhh");


        //if the device is registered

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        Profile_Pic =(AppCompatImageView)navhead.findViewById(R.id.ha_aciv_profile_pic);
        Vehicle_Pic =(AppCompatImageView)navhead.findViewById(R.id.ha_aciv_vehicle_pic);
        Profile_Vehical_Name =(AppCompatTextView) navhead.findViewById(R.id.ha_aciv_profile_name);
        //.....................................................................
        SetVehicleImageAndModel();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){


            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                SetTripTitle();
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                SetTripTitle();
                SetVehicleImageAndModel();
                //..........................................................................................
            }
        };

        drawer.setDrawerListener(toggle);
        toggle.syncState();

        Gpslocation myloc = new Gpslocation(Drawer.this);
        LatLng l = myloc.getlatlong();
        double lat = l.latitude;
        double lng = l.longitude;

      //  String add = myloc.getAddressFromLocation(l,Drawer.this,);


        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);

        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);

            if(addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("Address:\n");
                for(int i=0; i<returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
               // myAddress.setText(strReturnedAddress.toString());
               // Toast.makeText(Drawer.this, "Address : " + strReturnedAddress.toString(), Toast.LENGTH_SHORT).show();
               // System.out.println("Adderes " + strReturnedAddress.toString());
            }
            else{
               // Toast.makeText(Drawer.this, "No Address returned!", Toast.LENGTH_SHORT).show();
                //myAddress.setText("No Address returned!");
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //Toast.makeText(Drawer.this, "Canont get Address! " , Toast.LENGTH_SHORT).show();
            //myAddress.setText("Canont get Address!");
        }


//        Toast.makeText(Drawer.this, "drawerlat : " + d + " drawerLng : " + l, Toast.LENGTH_SHORT).show();
//        System.out.println("lat : " + d + " Lng : " + l);

        tripbutton = menu.findItem(R.id.nav_starttrip);


//        addvehicle = (LinearLayout) headerView.findViewById(R.id.addvehicle);
//        addvehicle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(getApplicationContext(), VehicleRegister.class);
//                startActivity(i);
//            }
//        });

        frag = new Frag_Dashboard();
        fm.beginTransaction().replace(R.id.fragment_frame, frag).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_notification) {
            Intent intent = new Intent(Drawer.this,NotiMessage.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    boolean IsOnTrip(){

        return mypref.getBoolean(Constants.IsTripStarted,false);

    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

//
//        Bundle bundle = new Bundle();
//        // bundle.putInt("hii",item.getItemId());
//        bundle.putInt("itemid", item.getItemId());
//        bundle.putString("itemtitle",String.valueOf(item));
//        Frag_Refuelling fragbundle = new Frag_Refuelling();
//        fragbundle.setArguments(bundle);
//
//        Log.v("55555555555555555555","this is drawer page"+ bundle);
//
//        fm.beginTransaction().replace(R.id.fragment_frame, fragbundle).commit();


        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_dashboard) {
            // Handle the camera action
            Frag_Dashboard fragbundle = new Frag_Dashboard();
            fm.beginTransaction().replace(R.id.fragment_frame, fragbundle).commit();

        } else if (id == R.id.nav_starttrip) {

            Frag_StartTripRecords fragbundle = new Frag_StartTripRecords();
            fm.beginTransaction().replace(R.id.fragment_frame, fragbundle).commit();


        } else if (id == R.id.nav_refuelling) {
            Frag_RefuellingRecords fragbundle = new Frag_RefuellingRecords();
            fm.beginTransaction().replace(R.id.fragment_frame, fragbundle).commit();

        } else if (id == R.id.nav_expenses) {
            Frag_ExpenseRecords fragbundle = new Frag_ExpenseRecords();
            fm.beginTransaction().replace(R.id.fragment_frame, fragbundle).commit();

        } else if (id == R.id.nav_service) {
            Frag_Servicerecords fragbundle = new Frag_Servicerecords();
            fm.beginTransaction().replace(R.id.fragment_frame, fragbundle).commit();

        } else if (id == R.id.nav_gpslogs) {
            Frag_GPSLogsRecords fragbundle = new Frag_GPSLogsRecords();
            fm.beginTransaction().replace(R.id.fragment_frame, fragbundle).commit();

        } else if (id == R.id.nav_contact) {

        } else if (id == R.id.nav_about) {
            Intent abintent = new Intent(Drawer.this,About.class);
            startActivity(abintent);

        }else if (id == R.id.nav_logout){
            LogoutApp(cid,did);
        }else if (id == R.id.nav_panic){
            Intent intentpanic = new Intent(Drawer.this,Panic.class);
            startActivity(intentpanic);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

   void SetTripTitle(){
       Log.wtf("Transport", "SetTripTitle: "+ IsOnTrip());
       if (IsOnTrip()) {
           tripbutton.setTitle(getResources().getString(R.string.stoptrip));
       } else {
           tripbutton.setTitle(getResources().getString(R.string.starttrip));
       }
   }
    void SetVehicleImageAndModel(){
        VImagePath = mypref.getString(Constants.KEY_VEHICLE_IMAGE,"");

        Picasso.with(Drawer.this).load(ProfilePath).transform(new CircleTransformNew()).resize(imgSize, imgSize).centerInside().into(Profile_Pic,new Callback() {
            @Override
            public void onSuccess() {
            }
            @Override
            public void onError() {
                Picasso.with(Drawer.this).load(R.drawable.common_ic_googleplayservices).transform(new CircleTransformNew()).resize(imgSize, imgSize).centerInside().into(Profile_Pic);
            }
        });
        if (VImagePath != ""){
            Picasso.with(Drawer.this).load(VImagePath).fit().into(Vehicle_Pic,new Callback() {
                @Override
                public void onSuccess() {
                }
                @Override
                public void onError() {
                    Picasso.with(Drawer.this).load(R.drawable.common_ic_googleplayservices).fit().into(Vehicle_Pic);
                }
            });
        }else{
            Picasso.with(Drawer.this).load(R.drawable.common_ic_googleplayservices).transform(new CircleTransformNew()).resize(imgSize, imgSize).centerInside().into(Vehicle_Pic);
        }
        String vmodel = mypref.getString(Constants.KEY_VEHICLE_MODEL,"");
        if (vmodel.equals("")){
            Profile_Vehical_Name.setText("No Vehículo");
        }else {
            Profile_Vehical_Name.setText(vmodel);
        }


    }

    public void LogoutApp(int cid, int did) {

        final ProgressDialog loading = ProgressDialog.show(this, "Cargando", "por favor espera...", false, false);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        final SharedPreferences.Editor editor = pref.edit();

        // OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .addConverterFactory(GsonConverterFactory.create())
                // .client(httpClient.build())
                .build();

        SlimIface service = retrofit.create(SlimIface.class);
        Call<LogoutModel> call = service.Logoutapp(cid, did);
        call.enqueue(new retrofit2.Callback<LogoutModel>() {
            @Override
            public void onResponse(Call<LogoutModel> call, Response<LogoutModel> response) {

                if (response != null) {
                    Log.wtf("Transport insert trip", "Response: " + response.message());
                    if (response.body().error.equals(false)) {  //insert Success

                        mypref.putInt(Constants.KEY_ISLOGIN,0);
                        mypref.commit();
                        Drawer.this.finish();

//                        Toast.makeText(Drawer.this, "Viaje iniciado con éxito", Toast.LENGTH_SHORT).show();
//                        mypref.putInt(Constants.KEY_TRIP_ID, response.body().tripid);
//                        mypref.commit();
//
//                        editor.putInt("Toggle", 1);
//                        mypref.putBoolean(Constants.IsTripStarted, true);
//                        mypref.commit();
//                        editor.commit();
//
//                        Intent i = new Intent(Drawer.this, Drawer.class);
//                        startActivity(i);
//                        Drawer.this.finish();
                        loading.dismiss();

                    } else // insert failure
                    {
                        loading.dismiss();
                        Toast.makeText(Drawer.this, "Algo salió mal ", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    loading.dismiss();
                    Toast.makeText(Drawer.this, response.body().toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LogoutModel> call, Throwable t) {
                loading.dismiss();
                String merror = t.getMessage();
                Toast.makeText(Drawer.this, merror, Toast.LENGTH_LONG).show();
                Log.wtf("Transport", "onFailure: " + t.getCause());
            }
        });
    }
}
