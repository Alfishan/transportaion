package com.xeme.transportation.AppDatabase;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.Date;

/**
 * Created by root on 28/6/16.
 */
public class ServiceModel extends BaseModel {
    @Column
    @PrimaryKey(autoincrement = true)
    public
    int id;

    @Column
    public
    Date date;

    @Column
    public
    Long time;

    @Column
    public
    String odometer;

    @Column
    public String typeofservice;

    @Column
    public String value;

    @Column
    public String levelofservice;

    @Column
    public String notes;

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setLevelofservice(String levelofservice) {
        this.levelofservice = levelofservice;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setTypeofservice(String typeofservice) {
        this.typeofservice = typeofservice;
    }

    public void setOdometer(String odometer) {
        this.odometer = odometer;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
