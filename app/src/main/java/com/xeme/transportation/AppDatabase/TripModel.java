package com.xeme.transportation.AppDatabase;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ModelContainer;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.Date;

/**
 * Created by root on 27/6/16.
 */
@Table(databaseName = AppDatabase.NAME)
@ModelContainer
public class TripModel extends BaseModel {
    @Column
    @PrimaryKey(autoincrement = true)
    public
    int id;

    @Column
    public
    String startlocation;

    @Column
    public
    String initialodometer;

    @Column
    public
    Date date;

    @Column
    public
    Long time;

    @Column
    public
    String destinationlocation;

    @Column
    public
    String reason;

    @Column
    public
    String notes;

    @Column
    public
    boolean isstart;

    public void setStartlocation(String startlocation) {
        this.startlocation = startlocation;
    }

    public void setInitialodometer(String initialodometer) {
        this.initialodometer = initialodometer;
    }

    public void setDestinationlocation(String destinationlocation) {
        this.destinationlocation = destinationlocation;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
    public void setIsstart(boolean isstart) {
        this.isstart = isstart;
    }


    public void setDate(String s) {
    }

    public void setTime(String s) {
    }
}
