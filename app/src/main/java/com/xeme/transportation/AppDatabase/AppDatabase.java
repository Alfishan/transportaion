package com.xeme.transportation.AppDatabase;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by root on 25/6/16.
 */
@Database(name = AppDatabase.NAME, version = AppDatabase.VERSION)
public class AppDatabase {

    public static final String NAME = "TransportationDatabase";
    public static final int VERSION = 1;

}
