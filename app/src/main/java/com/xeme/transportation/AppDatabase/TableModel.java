package com.xeme.transportation.AppDatabase;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ModelContainer;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by root on 25/6/16.
 */
@Table(databaseName = AppDatabase.NAME)
@ModelContainer
public class TableModel extends BaseModel{
    @Column
    @PrimaryKey
    public
    int id;

    @Column
    public
    String name;

    @Column
            public
    String pass;

    @Column
            public
    String imei;

    @Column
            public
    String careerprovider;

    public void setId(int id){
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPass(String pass){
        this.pass = pass;
    }

    public void setImei(String imei){
        this.imei = imei;
    }
    public void setCareerprovider(String careerprovider){
        this.careerprovider = careerprovider;
    }
}
